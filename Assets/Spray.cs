﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spray : MapObj {
	//// UI
	public TextMesh _txtSpraySize;
	public TextMesh _txtCurMeatBeanLeftTime;
	// end UI

	float m_fRotationZ = 0.0f;

    float m_fSprayInterval = 3f;
	float m_fPreSprayTime = 3f;
    float m_fDensity = 5.0f;
	float m_fMeatDensity = 5.0f;
    float m_fMaxDis = 2.0f;
	float m_fMinDis = 2.0f;
    float m_fBeanLifeTime = 1.0f;
	float m_fMeat2BeanSprayTime = 1f;
	float m_fSpraySize = 0f;
	float m_fDrawSpeed = 0f;
	float m_fDrawArea = 0f;
	float m_fPreDrawTime = 0f;
	float m_fDrawTime = 0f;
	float m_fDrawInterval;

	public GameObject _goMain;
    public GameObject _goSprayedBeansContainer;

	

	static Vector3 vecTempScale = new Vector3();
	static Vector3 vecTempRotation = new Vector3();
	static Vector3 vecTempPos = new Vector3();

    int m_nBeanCountForthornChance = 0;
    int m_nThornChance = 0;

	public CircleCollider2D _CollierMain; 
	public CircleCollider2D _CollierObsCircle;

    public int m_nGUID;

	float m_fCurMeatBeanLeftTime = 0f;



	// Use this for initialization
	void Start () {
	//	_CollierObsCircle.enabled = false;
	}

	void Awake()
	{
        Init();
	}

    void OnMouseDown()
    {
        if (MapEditor.s_Instance)
        {
            MapEditor.s_Instance.PickObj(this);
        }
    }

    void Init()
    {
        m_eObjType = eMapObjType.bean_spray;

        this.gameObject.name = "Spray_" + m_nGUID;

		//_obsceneCircle.SetColor ( Color.green );
		//if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game) {
		//	_CollierObsCircle.enabled = false;
		//}
    }

    // Update is called once per frame
    void Update () {

		if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.None) {
			return;
		}

		Rotate ();
		SprayBean ();
		SpryMeatBean ();
		PreDrawLoop ();
		DrawLoop ();
		CapturedLoop ();
		RefreshTextInfo ();
	}

	void Rotate()
	{
		float fRotation = Time.fixedDeltaTime;
		m_fRotationZ += fRotation;
        if (m_fRotationZ > 360.0f)
        {
            m_fRotationZ = 0.0f;
        }
       // _goMain.transform.localRotation = Quaternion.identity;
       // _goMain.transform.Rotate (0.0f, 0.0f, m_fRotationZ);
	}

	float m_fSprayTimeCount = 0.0f;
	static int s_Shit = 0;
    
    List<Thorn> m_lstThorns = new List<Thorn>();
	void SprayBean()
	{
        /*
        GameObject goBean = null;
        Bean bean = null;
        if ( AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game ) // 游戏模式
        { 
            //if (!PhotonNetwork.isMasterClient)
            //{
            //    return;
            //}
        }
        else // 地图编辑器模式
        {
            if ( MapEditor.s_Instance && !MapEditor.s_Instance.m_bShowRealtimeBeanSpray)
            {
               for ( int i = m_lstBeans.Count - 1; i >= 0; i-- )
                {
                    Bean node = m_lstBeans[i];
                    if ( node != null )
                    {
                        GameObject.Destroy( node.gameObject );
                    }
                }

               for ( int i = m_lstThorns.Count - 1; i >= 0; i-- )
                {
                    Thorn node = m_lstThorns[i];
                    if ( node )
                    {
                        GameObject.Destroy(node.gameObject);
                    }
                }

                return;
            }
        }

		if (m_fSprayTimeCount >= m_fPreSprayTime) {
			DoPreSpray ();
		}

        if (m_fSprayTimeCount < m_fSprayInterval) {
			m_fSprayTimeCount += Time.fixedDeltaTime;
			return;
		}

		EndPreSpray ();

		int nNum = (int)(m_fDensity * m_fSprayInterval); // m_fDensity是“每秒喷多少个”
        if (nNum < 1)
        {
            nNum = 1;
        }
        m_fSprayTimeCount = 0.0f;
		float fAngle = 360f / nNum;
        for ( int i = 0; i < nNum; i++ )
        {
			//GenerateOneBean( fAngle * i + m_fRotationZ );
        }




    
    */
    }

    float m_fMeatBeanSprayCount = 0f;
	void SpryMeatBean()
	{
        /*
		if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game) {
			m_fCurMeatBeanLeftTime -= Time.fixedDeltaTime;
			if (m_fCurMeatBeanLeftTime <= 0f) {
				m_fCurMeatBeanLeftTime = 0f;
				return;
			}
		}
		m_fMeatBeanSprayCount += Time.fixedDeltaTime;
		if (m_fMeatBeanSprayCount < m_fMeatBeanSprayInterval) {
			return;
		}
		m_fMeatBeanSprayCount = 0f;

		Bean bean = GenerateOneMeatBean ();
        */
	}

	bool m_bPreSpraying = false;
	void DoPreSpray()
	{
		if (m_bPreSpraying) {
			return;
		}
		m_bPreSpraying = true;

		vecTempScale = _goMain.transform.localScale;
		vecTempScale.x *= 0.8f;
		vecTempScale.y *= 0.8f;
		vecTempScale.z = 1f;
		_goMain.transform.localScale = vecTempScale;
	}

	void EndPreSpray()
	{
		m_bPreSpraying = false;
		vecTempScale = _goMain.transform.localScale;
		vecTempScale.x /= 0.8f;
		vecTempScale.y /= 0.8f;
		vecTempScale.z = 1f;
		_goMain.transform.localScale = vecTempScale;
	}
    /*
	Bean GenerateOneMeatBean()
	{
		Bean bean = null;

		GameObject goBean = GameObject.Instantiate((GameObject)Resources.Load("preBean"));
		m_lstBeans.Add(goBean.GetComponent<Bean>());

		vecTempPos = _goMain.transform.position;
		vecTempPos.z -= 1.0f;
		goBean.transform.position = vecTempPos;
		goBean.transform.parent = this.gameObject.transform;
		bean = goBean.GetComponent<Bean>();
		bean.SetLiveTime(m_fBeanLifeTime);
		bean._beanType = Bean.eBeanType.spray;
		bean._srMain.color = Color.red;

		float s = (float)UnityEngine.Random.Range(m_fMinDis, m_fMaxDis);
		float t = 2.0f;
		float v0 = CyberTreeMath.GetV0(s, t);
		float a = CyberTreeMath.GetA(s, t);
		//if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game) // 游戏模式
		//{
		//    bean.BeginSpray(v0, a, m_fRotationZ);
		//}
		//else// 地图编辑器模式
		//{

		bean.Local_BeginSpray(v0, a, m_fRotationZ);
		//}

		return bean;
	}
    */
	/*
	void  GenerateOneBean( float fRotationZ )
    {
        //GameObject goBean = null;
        //goBean = GameObject.Instantiate((GameObject)Resources.Load("preBean"));
		Bean bean = ResourceManager.ReuseBean();
		if (bean == null) {
			return;
		}
		m_lstBeans.Add(bean);
       

        vecTempPos = _goMain.transform.position;
        vecTempPos.z -= 1.0f;
		bean.transform.position = vecTempPos;
		bean.transform.parent = this.gameObject.transform;
        bean.SetLiveTime(m_fBeanLifeTime);
        bean._beanType = Bean.eBeanType.spray;

		float s = (float)UnityEngine.Random.Range(m_fMinDis, m_fMaxDis);
        float t = 2.0f;
        float v0 = CyberTreeMath.GetV0(s, t);
        float a = CyberTreeMath.GetA(s, t);
        //if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game) // 游戏模式
        //{
        //    bean.BeginSpray(v0, a, m_fRotationZ);
        //}
        //else// 地图编辑器模式
        //

		bean.Local_BeginSpray(v0, a,fRotationZ);
        //}

        m_nBeanCountForthornChance++;
        GenerateThorn();
    }
	*/
    void GenerateThorn()
    {
        if (m_nThornChance <= 0 )
        {
            return;
        }

        if ( m_nBeanCountForthornChance < m_nThornChance )
        {
            return;
        }
        m_nBeanCountForthornChance = 0;

        GameObject goThorn = null;
        goThorn = GameObject.Instantiate((GameObject)Resources.Load("hedge"));
        vecTempPos = _goMain.transform.position;
        vecTempPos.z -= 1.0f;
        goThorn.transform.position = vecTempPos;
        goThorn.transform.parent = this.gameObject.transform;
        Thorn thorn = goThorn.GetComponent<Thorn>();
        m_lstThorns.Add( thorn );
        thorn.SetLiveTime(m_fBeanLifeTime); // 为简化设计，暂时让喷泉中的豆子和刺采用同样的生命周期
        thorn.m_eObjType = eMapObjType.thorn;

        float s = (float)UnityEngine.Random.Range(1.0f, m_fMaxDis);// 为简化设计，暂时让喷泉中的豆子和刺采用同样的射程范围
        float t = 2.0f;
        float v0 = CyberTreeMath.GetV0(s, t);
        float a = CyberTreeMath.GetA(s, t);
        thorn.Local_BeginMove(v0, a, m_fRotationZ);
    }

    public void SetDensity( float fDensity )
    {
        m_fDensity = fDensity;

		// 废弃！ 喷射时间间隔专门配置
        // 通过密度系数算出喷射间隔时间   
        //m_fSprayInterval = 1.0f / m_fDensity;
    }

    public float GetDensity()
    {
        return m_fDensity;
    }

	public float GetSprayInterval()
	{
		return m_fSprayInterval;
	}

	public void SetSprayIntrerval( float val )
	{
		m_fSprayInterval = val;
		m_fPreSprayTime = m_fSprayInterval - 0.2f;
	}

	float m_fMeatBeanSprayInterval = 0f;
	public void SetMeatDensit( float val )
	{
		m_fMeatDensity = val;
		m_fMeatBeanSprayInterval = 1f / m_fMeatDensity;
	}

	public float GetMeatDensit()
	{
		return m_fMeatDensity;
	}

    public void SetMaxDis( float fMaxDis )
    {
        m_fMaxDis = fMaxDis;
    }

    public float GetMaxDis()
    {
        return m_fMaxDis;
    }

    public void SetBeanLifeTime( float fBeanLifeTime )
    {
        m_fBeanLifeTime = fBeanLifeTime;
    }

    public float GetBeanLifeTime()
    {
        return m_fBeanLifeTime;
    }

    public void SetThornChance( int nThornChance )
    {
        m_nThornChance = nThornChance;
    }

    public int GetThornChance()
    {
        return m_nThornChance;
    }

	public void SetMinDis( float val )
	{
		m_fMinDis = val;
	}

	public float GetMinDis()
	{
		return m_fMinDis;
	}

	public void SetMeat2BeanSprayTime( float val )
	{
		m_fMeat2BeanSprayTime = val;
	}

	public float GetMeat2BeanSprayTime()
	{
		return m_fMeat2BeanSprayTime;
	}

	float m_fRadius = 1.94f;
	public void SetSpraySize( float val )
	{
		m_fSpraySize = val;

		vecTempScale.x = val / 4f;
		vecTempScale.y = val / 4f;
		vecTempScale.z = 1f;
		_goMain.transform.localScale = vecTempScale;
		//_CollierMain.radius = _goMain.transform.localScale.x;

		//_txtSpraySize.text = "喷泉尺寸:" + m_fSpraySize.ToString ("f2");
	}

	public float GetSpraySize()
	{
		return m_fSpraySize;
	}

	public void SetDrawSpeed( float val )
	{
		m_fDrawSpeed = val;
	}

	public float GetDrawSpeed()
	{
		return m_fDrawSpeed;
	}

	public void SetDrawArea( float val )
	{
		m_fDrawArea = val;

		vecTempScale.x = val;
		vecTempScale.y = val;
		vecTempScale.z = 1f;
		//_obsceneCircle.transform.localScale = vecTempScale;
	}

	public float GetDrawArea()
	{
		return m_fDrawArea;
	}

	public void SetPreDrawTime( float val )
	{
		m_fPreDrawTime = val / 2f;
	}

	public float GetPreDrawTime()
	{
		return m_fPreDrawTime;
	}

	public void SetDrawTime( float val )
	{
		m_fDrawTime = val;
	}

	public float GetDrawTime()
	{
		return m_fDrawTime;
	}

	public void SetDrawInterval( float val )
	{
		m_fDrawInterval = val;
	}

	public float GetDrawInterval()
	{
		return m_fDrawInterval;
	}

	float m_fBeginPreDrawTime = 0f;
	int m_nDrawStatus = 0;
	public void BeginPreDraw( float fBeginPreDrawTime )
	{
		return; // 暂不开放吸球功能


	}

	void PreDrawLoop()
	{

	}

	public void BeginDraw ()
	{
		m_nDrawStatus = 2;
		m_fBeginDrawTime = m_fDrawTime;

		// 开启喷泉外环的碰撞体，开始捕捉球球
		_CollierObsCircle.enabled = true;
	}

	float m_fBeginDrawTime = 0f;
	void DrawLoop()
	{
		if (m_nDrawStatus != 2) {
			return;
		}

		m_fBeginDrawTime -= Time.fixedDeltaTime;
		if (m_fBeginDrawTime <= 0f) {
			EndDraw ();
		}

		for (int i = m_lstCapturedBalls.Count - 1; i >= 0; i--) {
			Ball ball = m_lstCapturedBalls [i];
			if (ball == null) {
				m_lstCapturedBalls.RemoveAt (i);
				continue;
			}

			if (ball.GetSize () >= GetSpraySize ()) {
				ReleaseOneBall ( ball );
				continue;
			}
		

		} // end for
	}

		void CapturedLoop()
		{
        /*
			for (int i = m_lstCapturedBalls.Count - 1; i >= 0; i--) {
				Ball ball = m_lstCapturedBalls [i];
				if (ball == null) {
					m_lstCapturedBalls.RemoveAt (i);
					continue;
				}

			if (ball.GetSize () >= GetSpraySize ()) {
				ReleaseOneBall ( ball );
				continue;
			}

				if (ball.IsCaptured ()) {
					vecTempPos = ball.transform.position;
					float fDelta = ball._fCapturedSpeed * Time.fixedDeltaTime;
					vecTempPos.x += fDelta * ball._fCapturedDirX;
					vecTempPos.y += fDelta * ball._fCapturedDirY;
					ball.Local_SetPos ( vecTempPos.x, vecTempPos.y, vecTempPos.z );
				}
		}
            */
	}

	void EndDraw()
	{
		if (PhotonNetwork.isMasterClient) {
			//BeginPreDraw ( (float )Main.GetTime() );
			Main.s_Instance.m_MainPlayer.BeginBeanSprayPreDraw( false, this.GetGUID() );
		}
	}

	List<Ball> m_lstCapturedBalls = new List<Ball>();
	public void CaptureOneBall( Ball ball )
	{
//		if (!ball.photonView.isMine) {
//			return;
//		}

		if ( ball.GetSize() >= GetSpraySize() )
		{
			return;
		}

		// 所谓帧同步，并非绝对意义上的同步，而是把效果调到所有客户端达成“共识”
		// 每个玩家都只会关注自己的球会不会被捕获，不会关注别人的球。所以球被捕获的事件有那个球的MainPlayer发起
//		Main.s_Instance.m_MainPlayer.CaptureOneBall( this.GetGUID(), ball.photonView.ownerId, ball.photonView.viewID );
	}

	public void DoCaptureOneBall (Ball ball)
	{
		m_lstCapturedBalls.Add ( ball );
	}

	public void ReleaseOneBall( Ball ball )
	{
//		if (!ball.photonView.isMine) {
//			return;
//		}

//		Main.s_Instance.m_MainPlayer.ReleaseOneBall ( this.GetGUID(), ball.photonView.ownerId, ball.photonView.viewID );
	}

	public void DoReleaseOneBall( Ball ball )
	{
	}

	
	public void Eat( float fSize )
	{
		Main.s_Instance.m_MainPlayer.SprayEatBall( GetGUID(), fSize  );
	}

	public void DoEat( float fSize )
	{
		m_fCurMeatBeanLeftTime += fSize * m_fMeat2BeanSprayTime;   
	}

	float m_fMeatBeanLeftTimeShowInterval = 0;
	void RefreshTextInfo()
	{
		m_fMeatBeanLeftTimeShowInterval += Time.fixedDeltaTime;
		if (m_fMeatBeanLeftTimeShowInterval < 1f) {
			return;
		}
		m_fMeatBeanLeftTimeShowInterval = 0f;

		//_txtCurMeatBeanLeftTime.text = "人肉豆子剩余时间:" + m_fCurMeatBeanLeftTime.ToString("f0") + "秒";


	}

	public void SetGUID( int nGUID )
	{
		m_nGUID = nGUID;
	}

	public int GetGUID()
	{
		return m_nGUID;
	}

	public int GetDrawStatus()
	{
		return m_nDrawStatus;
	}

	public void SetDrawStatus( int nDrawStatus, float fParam1, float  fParam2, float  fParam3 )
	{
        /*
		m_nDrawStatus = nDrawStatus;

		if (m_nDrawStatus == 1) {
			int nObsCircleStatus = (int)fParam1;
			float fObsCircleTimeCount = fParam2;
			_obsceneCircle.SetParams ( m_fDrawInterval, nObsCircleStatus, fObsCircleTimeCount );
		} else if (m_nDrawStatus == 2) {
			m_fBeginDrawTime = fParam1;
			_obsceneCircle.SetEnd (); 
		}

		m_fCurMeatBeanLeftTime = fParam3;
        */
	}


	public float GetPredrawBeginTime()
	{
		return m_fBeginPreDrawTime;
	}

	public void SetPredrawBeginTime( float val )
	{
		m_fBeginPreDrawTime = val;
	}



	public float GetDrawTimeLeft()
	{
		return m_fBeginDrawTime;
	}

	public float GetMeatBeanTimeLeft()
	{
		return m_fCurMeatBeanLeftTime;
	}

	// 吃掉球球，生成人肉豆子
	public void GenerateMeatBean( Ball ball )
	{
		if (ball.IsDead ()) {
			return;
		}

		float fDrawSize = 0.5f;
		float fBallSize = ball.GetSize ();
		float fLeftSize = fBallSize - fDrawSize;
		if (fLeftSize < Main.BALL_MIN_SIZE) {
			ball._Player.DeleteBall (ball.GetIndex ());
		} else {
			ball.SetSize ( fLeftSize );
		}

		DoEat ( fDrawSize );
	}
}
