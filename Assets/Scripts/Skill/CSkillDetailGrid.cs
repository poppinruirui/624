﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CSkillDetailGrid : MonoBehaviour {

    public Text _txtSpeedAffect;
    public Text _txtCD;
    public Text _txtDuration;
    public Text _txtMpCost;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ParseData(Dictionary<int, CSelectSkill.sSkillDetail> dicData, int nLevel, int nCurSelectSkillId)
    {
        /*
        CSelectSkill.sSkillDetail info = dicData[nLevel];
        _txtSpeedAffect.text =( (int)( info.fSpeedAffect * 100f ) ) + "%";
        _txtCD.text = info.fColdDown + "秒";
        _txtDuration.text = info.fDuration + "秒";
        _txtMpCost.text = info.fMpCost.ToString();

        if (nCurSelectSkillId == (int)CSkillSystem.eSkillId.i_merge)
        {
            _txtSpeedAffect.text = "（无）";
            _txtDuration.text = "（无）";
        }
        */

        CSelectSkill.sSkillDetail info = dicData[nLevel];
        //(CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[0] = 

        CSelectSkill.s_Instance.GetTitles()[0].text = "";
        CSelectSkill.s_Instance.GetTitles()[1].text = "";
        CSelectSkill.s_Instance.GetTitles()[2].text = "";
        CSelectSkill.s_Instance.GetTitles()[3].text = "";
        CSelectSkill.s_Instance.GetTitles()[4].text = "";
        CSelectSkill.s_Instance.GetTitles()[5].text = "";


   


        float fValue = 0f;
        switch (nCurSelectSkillId)
        {
            case 3: // 潜行
                {
                    CSelectSkill.s_Instance.GetTitles()[0].text = "蓝耗";
                    CSelectSkill.s_Instance.GetTitles()[1].text = "ColdDown";
                    CSelectSkill.s_Instance.GetTitles()[2].text = "持续时间";
                    CSelectSkill.s_Instance.GetTitles()[3].text = "速度提升百分比";
                    CSelectSkill.s_Instance.GetTitles()[4].text = "";
                    CSelectSkill.s_Instance.GetTitles()[5].text = "";

                    nLevel = nLevel - 1;
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[0].text = info.aryValues[0];
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[1].text = info.aryValues[1] + "秒"; ;
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[2].text = info.aryValues[3] + "秒";
                    float.TryParse(info.aryValues[2], out fValue);
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[3].text = fValue * 100 + "%";
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[4].text = "";
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[5].text = "";
                }
                break;
            case 4: // 变刺
                {
                    CSelectSkill.s_Instance.GetTitles()[0].text = "蓝耗";
                    CSelectSkill.s_Instance.GetTitles()[1].text = "ColdDown";
                    CSelectSkill.s_Instance.GetTitles()[2].text = "持续时间";
                  //  CSelectSkill.s_Instance.GetTitles()[3].text = "速度影响百分比";
                  //  CSelectSkill.s_Instance.GetTitles()[4].text = "前摇时间";
                 //   CSelectSkill.s_Instance.GetTitles()[5].text = "采用刺ID";

                    nLevel = nLevel - 1;
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[0].text = info.aryValues[0];
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[1].text = info.aryValues[1] + "秒"; ;
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[2].text = info.aryValues[4] + "秒";
                    float.TryParse(info.aryValues[3], out fValue);
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[3].text = "";
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[4].text = "";
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[5].text = "";
                }
                break;
            case 5: // 湮灭
                {
                    CSelectSkill.s_Instance.GetTitles()[0].text = "蓝耗";
                    CSelectSkill.s_Instance.GetTitles()[1].text = "ColdDown";
                    CSelectSkill.s_Instance.GetTitles()[2].text = "持续时间";
                  //  CSelectSkill.s_Instance.GetTitles()[3].text = "速度影响百分比";
                    CSelectSkill.s_Instance.GetTitles()[3].text = "湮灭比例";
                    CSelectSkill.s_Instance.GetTitles()[5].text = "";
                    CSelectSkill.s_Instance.GetTitles()[4].text = "";

                    nLevel = nLevel - 1;
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[0].text = info.aryValues[0];
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[1].text = info.aryValues[1] + "秒"; ;
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[2].text = info.aryValues[3] + "秒";
                    float.TryParse(info.aryValues[4], out fValue);
                  //  (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[3].text = fValue * 100 + "%";
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[3].text = -float.Parse(info.aryValues[2]) + "倍";
                    float.TryParse(info.aryValues[5], out fValue);
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[4].text = "";
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[5].text = "";

                }
                break;
            case 6: // 魔法盾
                {
                    CSelectSkill.s_Instance.GetTitles()[0].text = "蓝耗";
                    CSelectSkill.s_Instance.GetTitles()[1].text = "ColdDown";
                    CSelectSkill.s_Instance.GetTitles()[2].text = "持续时间";
                    //CSelectSkill.s_Instance.GetTitles()[3].text = "速度影响百分比";
                    CSelectSkill.s_Instance.GetTitles()[4].text = "";
                    CSelectSkill.s_Instance.GetTitles()[5].text = "";

                    nLevel = nLevel - 1;
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[0].text = info.aryValues[0];
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[1].text = info.aryValues[1] + "秒"; ;
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[2].text = info.aryValues[2] + "秒";
                    float.TryParse(info.aryValues[3], out fValue);
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[3].text = "";
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[4].text = "";
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[5].text = "";
                }
                break;

            case 7: // 秒合
                {
                    CSelectSkill.s_Instance.GetTitles()[0].text = "蓝耗";
                    CSelectSkill.s_Instance.GetTitles()[1].text = "ColdDown";
                    CSelectSkill.s_Instance.GetTitles()[2].text = "前摇时间";
                    CSelectSkill.s_Instance.GetTitles()[3].text = "";
                    CSelectSkill.s_Instance.GetTitles()[4].text = "";
                    CSelectSkill.s_Instance.GetTitles()[5].text = "";

                    nLevel = nLevel - 1;
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[0].text = info.aryValues[0];
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[1].text = info.aryValues[1] + "秒"; ;
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[2].text = info.aryValues[2] + "秒";
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[3].text = "";
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[4].text = "";
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[5].text = "";
                }
                break;

            case 8: // 狂暴
                {
                    CSelectSkill.s_Instance.GetTitles()[0].text = "蓝耗";
                    CSelectSkill.s_Instance.GetTitles()[1].text = "ColdDown";
                    CSelectSkill.s_Instance.GetTitles()[2].text = "持续时间";
                    CSelectSkill.s_Instance.GetTitles()[3].text = "速度提升百分比";
                    //CSelectSkill.s_Instance.GetTitles()[4].text = "分球技能CD减为几秒";
                   // CSelectSkill.s_Instance.GetTitles()[5].text = "膨胀技能CD减为几秒";

                    nLevel = nLevel - 1;
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[0].text = info.aryValues[0];
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[1].text = info.aryValues[1] + "秒"; ;
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[2].text = info.aryValues[5] + "秒";
                    float.TryParse(info.aryValues[2], out fValue);
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[3].text = fValue * 100 + "%";
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[4].text = "";
                   (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[5].text = "" ;
                }
                break;


            case 9: // 金壳
                {
                    CSelectSkill.s_Instance.GetTitles()[0].text = "蓝耗";
                    CSelectSkill.s_Instance.GetTitles()[1].text = "ColdDown";
                    CSelectSkill.s_Instance.GetTitles()[2].text = "持续时间";
                //    CSelectSkill.s_Instance.GetTitles()[3].text = "速度影响百分比";
                    CSelectSkill.s_Instance.GetTitles()[4].text = "";
                    CSelectSkill.s_Instance.GetTitles()[5].text = "";

                    nLevel = nLevel - 1;
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[0].text = info.aryValues[0];
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[1].text = info.aryValues[1] + "秒"; ;
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[2].text = info.aryValues[2] + "秒";
                    float.TryParse(info.aryValues[3], out fValue);
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[3].text = "";
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[4].text = "";
                    (CSelectSkill.s_Instance.m_lstValuesLevels[nLevel])[5].text = "";
                }
                break;
        }





    }
}
