﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class CCyberTreeList : MonoBehaviour {

    int m_bCurItemNum = 0;

    public float m_fItemSize = 0f;

    public GameObject m_goContainer;
    List<CCyberTreeListItem> m_lstItems = new List<CCyberTreeListItem>();

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempPos2 = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public float m_fMoveSpeed = 0.1f;


    public bool m_bVert = true;


    /// new 
    public float m_fStartTop = 632f;
    public float m_fMinTop = 556f;
    public float m_fStartBottom = 200f;
    public float m_fMaxBottom = 260f;
    /// end new



    public int m_nDontAutoBackIfItemsNumLessThan = 0;
    public float m_fAutoBackSpeed = 10f;

    public GraphicRaycaster _graphicRaycaster;
    public EventSystem eventSystem;

    // Use this for initialization
    void Start () {
        

    }

    // Update is called once per frame
    void Update () {



        Draging();
       
        if ( Input.GetMouseButtonDown(0) && CheckCyberTreeItem(Input.mousePosition))
        {
            BeginDrag();
        }

        if ( Input.GetMouseButtonUp(0) )
        {
            EndDrag();
        }

        Sliding();

        //// auto back
        BeginAutoBack();
        AutoBackLoop();
        ////



    }

    public bool CheckCyberTreeItem(Vector2 pos)
    {
        if (_graphicRaycaster == null || eventSystem == null)
        {
            return true;
        }

        List<GameObject> objList = new List<GameObject>();
        PointerEventData eventData = new PointerEventData(eventSystem);
        eventData.pressPosition = pos;
        eventData.position = pos;

        List<RaycastResult> list = new List<RaycastResult>();
        _graphicRaycaster.Raycast(eventData, list);
        if (list.Count > 0)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].gameObject.tag == "CyberTreeItem")
                {
                    return true;
                }
            }
        }
        else
        {

        }

        return false;
    }

    public void LoadItemsFromList( List<CCyberTreeListItem> lst )
    {
        for ( int i = 0; i < lst.Count; i++ )
        {
            AddItem( lst[i] );
        }
    }

    public void AddItem( CCyberTreeListItem item, float fScale = 1f )
    {
        int nIndex = m_lstItems.Count;
        if (m_bVert)
        {
            vecTempPos.x = 0f;
            vecTempPos.y = -m_fItemSize * nIndex;
        }
        else
        {
            vecTempPos.x = m_fItemSize * nIndex;
            vecTempPos.y = 0;
        }
        vecTempPos.z = 0f;
        item.transform.SetParent( m_goContainer.transform );
        item.SetLocalPos(vecTempPos);
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        item.transform.localScale = vecTempScale;
        item.gameObject.SetActive( true );
        m_lstItems.Add(item);
    }

    public void GetItemList( ref List<CCyberTreeListItem> lst )
    {
        lst = m_lstItems;
    }

    public void ClearItems()
    {
        for ( int i = 0; i < m_lstItems.Count; i++ )
        {
            m_lstItems[i].gameObject.SetActive( false );
        }
        m_lstItems.Clear();
    }

    bool m_bDraging = false;
    Vector3 m_vecLastFrameMousePos = new Vector3();
    void BeginDrag()
    {
        EndAutoBack();
        EndSlide();
        m_bDraging = true;
        m_vecLastFrameMousePos = Input.mousePosition;
    }

    Vector3 m_vecLastMovement = new Vector3();
    void EndDrag()
    {
        m_bDraging = false;

        BeginSlide();
    }

    bool m_bSliding = false;
    float m_fV0 = 0f;
    float m_fA = 0f;
    void BeginSlide()
    {
        if ( (m_bVert && m_vecLastMovement.y < 100 ) || (!m_bVert && m_vecLastMovement.x < 100))
        {
            return;
        }
        float k = 10f;
        float s = m_bVert ? ( k * m_vecLastMovement.y ) : ( k * m_vecLastMovement.x );
        float t = 1.0f;
        m_fV0 = CyberTreeMath.GetV0( s, t );
        m_fA = CyberTreeMath.GetA( s, t );


        m_bSliding = true;
    }

    void Sliding()
    {
        if (!m_bSliding)
        {
            return;
        }

        float fV0 = m_fV0;
        float k = 0;
        vecTempPos = m_goContainer.transform.localPosition;
        if (m_bVert)
        {
            vecTempPos.y +=  m_fV0 * Time.deltaTime;
        }
        else
        {
            if ( m_bExceedLeftThreshold && m_fV0 > 0)
            {
                fV0 = 0;
            }

            if (m_bExceedRightThreshold && m_fV0 < 0)
            {
                fV0 = 0f;
            }

            vecTempPos.x += fV0 * Time.deltaTime;
        }
        m_fV0 += m_fA * Time.deltaTime;
        if ( (m_fA > 0 && m_fV0 >= 0 ) || (m_fA < 0 && m_fV0 <= 0))
        {
            EndSlide();
        }
        m_goContainer.transform.localPosition = vecTempPos;
    }

    void EndSlide()
    {
        m_bSliding = false;
    }

    int m_nAutoBackDir = 0;
    public float m_fMaxExceedAmount = 30f;
    bool m_bExceedLeftThreshold = false;
    float m_fExceedLeftAmount = 0f;
    bool m_bExceedRightThreshold = false;
    float m_fExceedRightAmount = 0f;

    bool m_bExceedTopThreshold = false;
    bool m_bExceedBottomThreshold = false;

    void Draging()
    {
        if (!m_bDraging)
        {
            return;
        }

       
        vecTempPos = m_goContainer.transform.localPosition;
        m_vecLastMovement = Input.mousePosition - m_vecLastFrameMousePos;
        if (m_bVert)
        {
            if (m_vecLastMovement.y < 0)
            {
              
                if ( GetFirstItemPosition().y <= m_fMinTop )
                {
                    
                }
                else
                {
                    vecTempPos.y += m_vecLastMovement.y * m_fMoveSpeed;
                }
            }
            else if (m_vecLastMovement.y > 0)
            {
                if (GetLastItemPosition().y >= m_fMaxBottom )
                {

                }
                else
                {
                    vecTempPos.y += m_vecLastMovement.y * m_fMoveSpeed;
                }
            }
            
        }
        else
        {
            vecTempPos.x += m_vecLastMovement.x * m_fMoveSpeed;
        }

        m_goContainer.transform.localPosition = vecTempPos;

        m_vecLastFrameMousePos = Input.mousePosition;
        /*
        if ( !m_bDraging )
        {
            return; 
        }

        float fSpeed = m_fMoveSpeed;
        

        m_vecLastMovement = Input.mousePosition - m_vecLastFrameMousePos;

        if (m_bExceedLeftThreshold && m_vecLastMovement.x > 0)
        {
            fSpeed = 0f;
        }

        if (m_bExceedRightThreshold && m_vecLastMovement.x < 0)
        {
            fSpeed = 0f;
        }

        vecTempPos = m_goContainer.transform.localPosition;
        if (m_bVert)
        {
            vecTempPos.y += m_vecLastMovement.y * fSpeed;
        }
        else
        {
            vecTempPos.x += m_vecLastMovement.x * fSpeed;
        }
        m_goContainer.transform.localPosition = vecTempPos;

        m_vecLastFrameMousePos = Input.mousePosition;
        */
    }

    public Vector3 GetLastItemPosition()
    {
        if (m_lstItems.Count == 0)
        {
            return Vector3.zero;
        }

        return m_lstItems[m_lstItems.Count - 1].GetComponent<RectTransform>().position;
    }

    public Vector3 GetFirstItemPosition()
    {
        if (m_lstItems.Count == 0)
        {
            return Vector3.zero;
        }

        return m_lstItems[0].GetComponent<RectTransform>().position;
    }

    void BeginAutoBack()
    {
        if (m_lstItems.Count < m_nDontAutoBackIfItemsNumLessThan)
        {
            return;
        }

        if ( Input.GetMouseButton(0) )
        {
            return;
        }

        if ( Mathf.Abs(m_fV0) > 0 )
        {
            return;
        }

        if (!m_bVert)
        {
            /*
            if (m_goContainer.transform.localPosition.x > m_fLeftStartPos)
            {
                m_nAutoBackDir = -1;
            }

            if (GetLastItemPosition().x < m_fRightStartPos)
            {
                m_nAutoBackDir = 1;
            }
            */
        }
        else
        {

        }
    }

    void AutoBackLoop()
    {
        if (m_nAutoBackDir == 0)
        {
            return;
        }

        if (m_bVert)
        {

        }
        else
        {
            vecTempPos = m_goContainer.transform.localPosition;
            vecTempPos.x += m_nAutoBackDir * m_fAutoBackSpeed * Time.deltaTime;

            m_goContainer.transform.localPosition = vecTempPos;

            /*
            if (m_nAutoBackDir == -1)
            {
                if (m_goContainer.transform.localPosition.x <= m_fLeftStartPos)
                {
                    EndAutoBack();
                }
            }
            else if (m_nAutoBackDir == 1)
            {
                if (  GetLastItemPosition().x >= m_fRightStartPos)
                {
                    EndAutoBack();
                }
            }
            */
         
        }        
    }

    void EndAutoBack()
    {
    m_nAutoBackDir = 0;
    }


}
