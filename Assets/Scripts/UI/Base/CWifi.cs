﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CWifi : MonoBehaviour {

    public GameObject[] m_aryGrid;

	// Use this for initialization
	void Start () {
        Texture2D t2d = new Texture2D(800, 600, TextureFormat.RGB24, false);
        RenderTexture rt;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateWifi( float fPingTime )
    {
        if (fPingTime < 50f)
        {
            Show(2);
        }
        else if (fPingTime < 100f)
        {
            Show(1);
        }
        else if (fPingTime < 150f)
        {
            Show(0);
        }
        else
        {
            Show(-1);
        }

    }

    void Show( int nLevel )
    {
        for ( int i = 0; i < m_aryGrid.Length; i++ )
        {
            m_aryGrid[i].SetActive( false);
        }
        if (nLevel == -1)
        {

        }
        else
        {
            m_aryGrid[nLevel].SetActive(true);
        }


    }
}
