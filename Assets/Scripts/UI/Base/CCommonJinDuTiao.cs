﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CCommonJinDuTiao : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public Text _txtInfo;
    public Text _txtJinDu;
    public Image _imgJinDu;

    public GameObject _containerWave;

    float m_fCurPercent = 0f;

    public GameObject m_preBoWen;
    public GameObject m_preBoWenGroup;

    public float m_fStartPosX = -1242f;
    public float m_fIntervalX = 254f;
    public float m_fPosY = 311f;

    // Use this for initialization
    void Start () {

        /*
        for ( int i = 0; i < 20; i++ )
        {
            CBoWenGroup goBoWenGroup = GameObject.Instantiate(m_preBoWenGroup).GetComponent<CBoWenGroup>();
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            goBoWenGroup.transform.SetParent(_containerWave.transform );
            goBoWenGroup.transform.localScale = vecTempScale;
            goBoWenGroup.InitBoWen(m_preBoWen);
            vecTempPos.x = m_fStartPosX + i * m_fIntervalX;
            vecTempPos.y = m_fPosY;
            vecTempPos.z = 0f;
            goBoWenGroup.transform.localPosition = vecTempPos;
        }
        */
    }
	
	// Update is called once per frame
	void Update () {
      

    }

    public void SetCurPercent( float fPercent )
    {
        m_fCurPercent = fPercent;
        if (_txtJinDu != null)
        {
            _txtJinDu.text = fPercent.ToString("f0") + "%";
        }
        _imgJinDu.fillAmount = m_fCurPercent / 100f;
    }

    public void SetInfo( string szInfo )
    {
        if (_txtInfo == null)
        {
            return;
        }
        _txtInfo.text = szInfo;
    }
}
