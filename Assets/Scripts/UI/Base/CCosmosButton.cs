﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CCosmosButton : Button {

	// Use this for initialization
	void Start () {
        this.onClick.AddListener( OnClickMe );

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickMe()
    {
        // 播放默认的按钮音效
        CAudioManager.s_Instance.PlaySE(CAudioManager.eSE.anniu);
    }
}
