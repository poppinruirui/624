﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISkillConfig : MonoBehaviour {
    /*
        public enum eSkillId
        {
            q_spore,
            w_spit,
            e_unfold,
            r_split,
            t_half_spit,
        };
    */
    public const int PARAM_NUM = 6;
    public const int LEVEL_NUM = 5;

    public InputField[] _aryValue0;
    public InputField[] _aryValue1;
    public InputField[] _aryValue2;
    public InputField[] _aryValue3;
    public InputField[] _aryValue4;

    List<InputField[]> m_lstInputFieldValues = new List<InputField[]>();
    List<string[]> m_lstValues = new List<string[]>();

    private void Awake()
    {
        Init();
    }

    bool m_bInited = false;
    void Init()
    {
        m_lstInputFieldValues.Add(_aryValue0);
        m_lstInputFieldValues.Add(_aryValue1);
        m_lstInputFieldValues.Add(_aryValue2);
        m_lstInputFieldValues.Add(_aryValue3);
        m_lstInputFieldValues.Add(_aryValue4);
        
        for (int i = 0; i < LEVEL_NUM; i++)
        {
            string[] aryValues = new string[PARAM_NUM];
            m_lstValues.Add(aryValues);
        }
        m_bInited = true;
    }

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnInputValueChanged_0_0()
    {
            m_lstValues[0][0] = m_lstInputFieldValues[0][0].text;
    }

    public void OnInputValueChanged_0_1()
    {
        m_lstValues[0][1] = m_lstInputFieldValues[0][1].text;
    }

    public void OnInputValueChanged_0_2()
    {
        m_lstValues[0][2] = m_lstInputFieldValues[0][2].text;
    }

    public void OnInputValueChanged_0_3()
    {
        m_lstValues[0][3] = m_lstInputFieldValues[0][3].text;
    }

    public void OnInputValueChanged_0_4()
    {
        m_lstValues[0][4] = m_lstInputFieldValues[0][4].text;
    }

    public void OnInputValueChanged_0_5()
    {
        m_lstValues[0][5] = m_lstInputFieldValues[0][5].text;
    }
    // ----- 1

    public void OnInputValueChanged_1_0()
    {
        m_lstValues[1][0] = m_lstInputFieldValues[1][0].text;
    }

    public void OnInputValueChanged_1_1()
    {
        m_lstValues[1][1] = m_lstInputFieldValues[1][1].text;
    }

    public void OnInputValueChanged_1_2()
    {
        m_lstValues[1][2] = m_lstInputFieldValues[1][2].text;
    }

    public void OnInputValueChanged_1_3()
    {
        m_lstValues[1][3] = m_lstInputFieldValues[1][3].text;
    }

    public void OnInputValueChanged_1_4()
    {
        m_lstValues[1][4] = m_lstInputFieldValues[1][4].text;
    }
    public void OnInputValueChanged_1_5()
    {
        m_lstValues[1][5] = m_lstInputFieldValues[1][5].text;
    }
    /// ---------- 2
    /// 
    public void OnInputValueChanged_2_0()
    {
        m_lstValues[2][0] = m_lstInputFieldValues[2][0].text;
    }

    public void OnInputValueChanged_2_1()
    {
        m_lstValues[2][1] = m_lstInputFieldValues[2][1].text;
    }

    public void OnInputValueChanged_2_2()
    {
        m_lstValues[2][2] = m_lstInputFieldValues[2][2].text;
    }

    public void OnInputValueChanged_2_3()
    {
        m_lstValues[2][3] = m_lstInputFieldValues[2][3].text;
    }

    public void OnInputValueChanged_2_4()
    {
        m_lstValues[2][4] = m_lstInputFieldValues[2][4].text;
    }
    public void OnInputValueChanged_2_5()
    {
        m_lstValues[2][5] = m_lstInputFieldValues[2][5].text;
    }
    /// ---- 3
    ///  
    public void OnInputValueChanged_3_0()
    {
        m_lstValues[3][0] = m_lstInputFieldValues[3][0].text;
    }

    public void OnInputValueChanged_3_1()
    {
        m_lstValues[3][1] = m_lstInputFieldValues[3][1].text;
    }

public void OnInputValueChanged_3_2()
{
        m_lstValues[3][2] = m_lstInputFieldValues[3][2].text;
    }

public void OnInputValueChanged_3_3()
{
        m_lstValues[3][3] = m_lstInputFieldValues[3][3].text;
    }

public void OnInputValueChanged_3_4()
{
        m_lstValues[3][4] = m_lstInputFieldValues[3][4].text;
    }
    public void OnInputValueChanged_3_5()
    {
        m_lstValues[3][5] = m_lstInputFieldValues[3][5].text;
    }

    /// <summary>
    /// / 4
    /// </summary>
    public void OnInputValueChanged_4_0()
{
        m_lstValues[4][0] = m_lstInputFieldValues[4][0].text;
    }

    public void OnInputValueChanged_4_1()
    {
        m_lstValues[4][1] = m_lstInputFieldValues[4][1].text;
    }

    public void OnInputValueChanged_4_2()
    {
        m_lstValues[4][2] = m_lstInputFieldValues[4][2].text;
    }

    public void OnInputValueChanged_4_3()
    {
        m_lstValues[4][3] = m_lstInputFieldValues[4][3].text;
    }

    public void OnInputValueChanged_4_4()
    {
        m_lstValues[4][4] = m_lstInputFieldValues[4][4].text;
    }
    public void OnInputValueChanged_4_5()
    {
        m_lstValues[4][5] = m_lstInputFieldValues[4][5].text;
    }

    /// <returns></returns>
    public string GetConfigContent()
    {
        if ( !m_bInited )
        {
            Init();
        }

        string szContent = "";
        bool bFirst = true;
        for (int i = 0; i < LEVEL_NUM; i++)
        {
            for (int j = 0; j < PARAM_NUM; j++)
            {
                if (bFirst)
                {
                    bFirst = false;
                }
                else
                {
                    szContent += ",";
                }
                szContent += m_lstValues[i][j];
            } // end for i 
        } // end for j
        return szContent;
    }


    public int GetMaxPoint()
    {
        int nMaxPoint = 0;

        for ( int i = 0; i < m_lstSkillParam.Count; i++ )
        {
            CSkillSystem.sSkillParam param = m_lstSkillParam[i];
            if (param.bAvailable)
            {
                nMaxPoint++;
            }
        }

        return nMaxPoint;
    }

    public void SetConfigContent( string[] aryValues, CSkillSystem.eSkillId eSkillId )
    {
        if ( !m_bInited )
        {
            Init();
        }

        int idx = 0;
        for (int i = 0; i < LEVEL_NUM; i++)
        {
            string[] ary_values = m_lstValues[i];
            InputField[] ary_inputs = m_lstInputFieldValues[i];
            for (int j = 0; j < PARAM_NUM; j++)
            {
                ary_values[j] = aryValues[idx];
                ary_inputs[j].text = ary_values[j];
                idx++;
            } // end j
            m_lstValues[i] = ary_values;
        } // end i

        TryParseData(eSkillId);
     }


    public void TryParseData(CSkillSystem.eSkillId  eSkillId)
    {
        float val = 0f;
        for (int i = 0; i < LEVEL_NUM; i++)
        {
            CSkillSystem.sSkillParam param = new CSkillSystem.sSkillParam();
            param.aryValues = new float[PARAM_NUM];
            param.bAvailable = false;
            for ( int j = 0; j < PARAM_NUM; j++ )
            {
                if (eSkillId == CSkillSystem.eSkillId.q_spore && j == 5) // 特殊情况，Monster的ID是用字符串来表示的
                {
                    param.szValue = m_lstValues[i][j];
                }
                else if (eSkillId == CSkillSystem.eSkillId.t_become_thorn && j == 5)
                {
                    param.szValue = m_lstValues[i][j];
                }
                else
                {
                    if ( float.TryParse(m_lstValues[i][j], out val) )
                    {
                        param.aryValues[j] = val;
                        param.bAvailable = true; // 只要解析成功了一个参数，则说明这个等级可用
                    }
                    else // 解析失败
                    {
                        
                    }
                    
                }
            } // end j
            param.nCurLevel = i + 1;
            m_lstSkillParam.Add(param);
        } // end i
                
    }
    
    List<CSkillSystem.sSkillParam> m_lstSkillParam = new List<CSkillSystem.sSkillParam>();

    public CSkillSystem.sSkillParam GetSkillParamByLevel( int nSkillLevel )
    {
        int key = nSkillLevel - 1;
        if ( key < 0 )
        {
            key = 0;
        }
        
        return m_lstSkillParam[key];
    }
}
