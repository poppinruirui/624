﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
public class CMonsterEditor : MonoBehaviour {

    public Sprite[] m_aryBeanPic;

    public Sprite[] m_arySprites;
    public Sprite[] m_arySprites_Thorn;

    public Dropdown _dropdownThorszId;
    public Dropdown _dropdownThornType;
    public Dropdown _dropdownSkill;
    public InputField _inputfieldThornDesc;
	public InputField _inputfieldThornFoodSize;
	public InputField _inputfieldSelfSize;
	public InputField _inputfieldThornColor;
	public InputField _inputfieldThornExp;
    public InputField _inputfieldThornMoney;
    public InputField _inputfieldThornBuffId;
	public InputField _inputfieldThornExplodeChildNum;
	public InputField _inputfieldThornExplodeMotherLeftPercent;
	public InputField _inputfieldThornExplodeRunDistance;
	public InputField _inputfieldThornExplodeRunTime;
	public InputField _inputfieldThornExplodeStay;
	public InputField _inputfieldThornExplodeShellTime;
	public InputField _inputfieldThornExplodeFormatioszId;
    public InputField _inputfieldThornSkillName;

    static Vector3 vecTempPos = new Vector3();

    public static CMonsterEditor s_Instance;
	void Awake()
	{
		s_Instance = this;
	}

	// Use this for initialization
	void Start () {

        //m_CurThornConfig = GetThornConfigById (0);
        m_CurThornConfig = GetMonsterConfigById( 0, 0 );

        InitDropDown_ThorszId();
        InitDropDown_ThornType ();
        InitDropDown_Skill();


    }
	
	// Update is called once per frame
	void Update () {
        BeanInteractBallLoop();
        BeanRebornLoop();

        /*
        GeneratingBean();

        for (int i = 0; i < 5; i++)
        {
            InitingBeanCollection_Loop();
        }
        */
    }
		

	void InitDropDown_ThorszId()
	{
		List<string> showNames = new List<string>();

		showNames.Add( "0号");
		showNames.Add("1号");
		showNames.Add("2号");
		showNames.Add("3号");
		showNames.Add("4号");
		showNames.Add("5号");
		showNames.Add("6号");
		showNames.Add("7号");
        showNames.Add("8号");
        showNames.Add("9号");
        showNames.Add("10号");
        showNames.Add("11号");
        showNames.Add("12号");
        showNames.Add("13号");
        showNames.Add("14号");
        showNames.Add("15号");
        showNames.Add("16号");
        showNames.Add("17号");
        showNames.Add("18号");

        UIManager.UpdateDropdownView( _dropdownThorszId, showNames);
	}

    void InitDropDown_ThornType()
    {
        List<string> showNames = new List<string>();

        showNames.Add("0 - 刺");
        showNames.Add("1 - 豆子");
        showNames.Add("2 - 场景球");
        showNames.Add("3 - 孢子");
        showNames.Add("4 - 捡拾技能");

        UIManager.UpdateDropdownView(_dropdownThornType, showNames);
    }


    void InitDropDown_Skill()
    {
        List<string> showNames = new List<string>();

        showNames.Add("潜");
        showNames.Add("刺");
        showNames.Add("湮");
        showNames.Add("免");
        showNames.Add("秒");
        showNames.Add("暴");
        showNames.Add("金");
        UIManager.UpdateDropdownView(_dropdownSkill, showNames);
    }


    public enum eMonsterBuildingType
    {
        thorn,            // 刺
        bean,             // 豆子
        scene_ball,     // 场景野球
        spore,            // 孢子
        pick_skill,            // 捡拾类技能
    };

	public struct sThornConfig
	{
		public string szId;
        public int nType;
		public float fFoodSize;
		public string szColor;
		public string szDesc;
		public float fSelfSize;
		public float fExp;
        public float fMoney;
        public float fBuffId;
		public float fExplodeChildNum;
		public float fExplodeMotherLeftPercent;
		public float fExplodeRunDistance;
		public float fExplodeRunTime;
		public float fExplodeStayTime;
		public float fExplodeShellTime;
		public float fExplodeFormationId;
        public int nSkillId;
    };

    /// <summary>
    /// / New
    /// </summary>
    Dictionary<string, sThornConfig> m_dicMonsterConfig = new Dictionary<string, sThornConfig>();
    sThornConfig tempMonsterConfig;

    public sThornConfig GetMonsterConfigById( int nType, int nId )
    {
        string szId = nType + "_" + nId;
        if (!m_dicMonsterConfig.TryGetValue(szId, out tempMonsterConfig))
        {
            tempMonsterConfig = new sThornConfig();
            tempMonsterConfig.nSkillId = CSkillSystem.SELECTABLE_SKILL_START_ID;
            tempMonsterConfig.szId = szId;
            tempMonsterConfig.nType = nType;
            m_dicMonsterConfig[szId] = tempMonsterConfig;
        }

        return tempMonsterConfig;
    }

    public sThornConfig GetMonsterConfigById( string  szId )
    {
        if ( !m_dicMonsterConfig.TryGetValue(szId, out tempMonsterConfig))
        {
            Debug.LogError( "没找到该ID号的配置： " + szId);
            return tempMonsterConfig;
        }
        string[] ary = szId.Split( '_' );
        tempMonsterConfig.nType = int.Parse(ary[0]);
        m_dicMonsterConfig[szId] = tempMonsterConfig;
        return tempMonsterConfig;
    }

    public sThornConfig NewMonsterConfig( string szId )
    {
        sThornConfig config = new sThornConfig();
        config.szId = szId;
        string[] ary = szId.Split( '_' );
        int nType = int.Parse( ary[0] );
        config.nType = nType;
        m_dicMonsterConfig[szId] = config;
        return config;
    }

    /// end New

    sThornConfig m_CurThornConfig;

    public Sprite GetSpriteByMonsterType( eMonsterBuildingType type )
    {
        return m_arySprites[(int)type];
    }
/*
	public sThornConfig GetThornConfigById( int szId )
	{
		sThornConfig config;
		if (!m_dicMonsterConfig.TryGetValue (szId, out config)) {
			config = new sThornConfig ();
			config.szId = szId;
			m_dicMonsterConfig [szId] = config;
		}
		return config;
	}
*/

    public void OnDropdownValueChanged_MonsterType()
    {
        m_CurThornConfig = GetMonsterConfigById(_dropdownThornType.value, _dropdownThorszId.value);
        UpdateContentUI();
    }

    public void OnDropdownValueChanged_ThorszId()
	{
        m_CurThornConfig = GetMonsterConfigById(_dropdownThornType.value, _dropdownThorszId.value);
        UpdateContentUI();
    }

    public void OnDropdownValueChanged_Skill()
    {
        m_CurThornConfig.nSkillId = _dropdownSkill.value + CSkillSystem.SELECTABLE_SKILL_START_ID;
        m_dicMonsterConfig[m_CurThornConfig.szId] = m_CurThornConfig;
    }


    void UpdateContentUI()
    {
        _inputfieldThornDesc.text = m_CurThornConfig.szDesc;
        _inputfieldThornFoodSize.text = m_CurThornConfig.fFoodSize.ToString();
        _inputfieldSelfSize.text = m_CurThornConfig.fSelfSize.ToString();
        _inputfieldThornColor.text = m_CurThornConfig.szColor;
        _inputfieldThornExp.text = m_CurThornConfig.fExp.ToString();
        _inputfieldThornMoney.text = m_CurThornConfig.fMoney.ToString();
        _inputfieldThornBuffId.text = m_CurThornConfig.fBuffId.ToString();
        _inputfieldThornExplodeChildNum.text = m_CurThornConfig.fExplodeChildNum.ToString();
        _inputfieldThornExplodeMotherLeftPercent.text = m_CurThornConfig.fExplodeMotherLeftPercent.ToString();
        _inputfieldThornExplodeRunDistance.text = m_CurThornConfig.fExplodeRunDistance.ToString();
        _inputfieldThornExplodeRunTime.text = m_CurThornConfig.fExplodeRunTime.ToString();
        _inputfieldThornExplodeStay.text = m_CurThornConfig.fExplodeStayTime.ToString();
        _inputfieldThornExplodeShellTime.text = m_CurThornConfig.fExplodeShellTime.ToString();
        _inputfieldThornExplodeFormatioszId.text = m_CurThornConfig.fExplodeFormationId.ToString();
        _dropdownSkill.value = m_CurThornConfig.nSkillId - CSkillSystem.SELECTABLE_SKILL_START_ID;
    }

	public void OnInputValueChanged_Thorn_SelfSize()
	{
		m_CurThornConfig.fSelfSize = float.Parse (_inputfieldSelfSize.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_FoodSize()
	{
		m_CurThornConfig.fFoodSize = float.Parse (_inputfieldThornFoodSize.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_Desc()
	{
		m_CurThornConfig.szDesc = _inputfieldThornDesc.text;
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_Color()
	{
		m_CurThornConfig.szColor = _inputfieldThornColor.text;
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_Exp()
	{
		m_CurThornConfig.fExp = float.Parse (_inputfieldThornExp.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

    public void OnInputValueChanged_Thorn_Money()
    {
        m_CurThornConfig.fMoney = float.Parse(_inputfieldThornMoney.text);
        m_dicMonsterConfig[m_CurThornConfig.szId] = m_CurThornConfig;
    }

    public void OnInputValueChanged_Thorn_BuffId()
	{
		m_CurThornConfig.fBuffId = float.Parse (_inputfieldThornBuffId.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_ExplodeChildNum()
	{
		m_CurThornConfig.fExplodeChildNum = float.Parse (_inputfieldThornExplodeChildNum.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_ExplodeMotherLeftPercent()
	{
		m_CurThornConfig.fExplodeMotherLeftPercent = float.Parse (_inputfieldThornExplodeMotherLeftPercent.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_ExplodeRunDistance()
	{
		m_CurThornConfig.fExplodeRunDistance = float.Parse (_inputfieldThornExplodeRunDistance.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_ExplodeRunTime()
	{
		m_CurThornConfig.fExplodeRunTime = float.Parse (_inputfieldThornExplodeRunTime.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_ExplodeStayTime()
	{
		m_CurThornConfig.fExplodeStayTime = float.Parse (_inputfieldThornExplodeStay.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_ExplodeShellTime()
	{
		m_CurThornConfig.fExplodeShellTime = float.Parse (_inputfieldThornExplodeShellTime.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_FormatioszId()
	{
		m_CurThornConfig.fExplodeFormationId = float.Parse (_inputfieldThornExplodeFormatioszId.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void SaveThorn(XmlDocument xmlDoc, XmlNode node )
	{
		foreach (KeyValuePair<string, sThornConfig> pair in m_dicMonsterConfig) {
			XmlNode nodeThorn =  StringManager.CreateNode (xmlDoc, node, "T" + pair.Value.szId);
			StringManager.CreateNode (xmlDoc, nodeThorn, "ID",  pair.Value.szId );
            StringManager.CreateNode(xmlDoc, nodeThorn, "Type", pair.Value.nType.ToString());
            StringManager.CreateNode (xmlDoc, nodeThorn, "Desc",  pair.Value.szDesc );
			StringManager.CreateNode (xmlDoc, nodeThorn, "FoodSize",  pair.Value.fFoodSize.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "SelfSize",  pair.Value.fSelfSize.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "Color",  pair.Value.szColor );
			StringManager.CreateNode (xmlDoc, nodeThorn, "Exp",  pair.Value.fExp.ToString() );
            StringManager.CreateNode(xmlDoc, nodeThorn, "Money", pair.Value.fMoney.ToString());
            StringManager.CreateNode (xmlDoc, nodeThorn, "BuffId",  pair.Value.fBuffId.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "ExplodeChildNum",  pair.Value.fExplodeChildNum.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "ExplodeMotherLeftPercent",  pair.Value.fExplodeMotherLeftPercent.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "ExplodeRunDistance",  pair.Value.fExplodeRunDistance.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "ExplodeRunTime",  pair.Value.fExplodeRunTime.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "ExplodeStayTime",  pair.Value.fExplodeStayTime.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "ExplodeShellTime",  pair.Value.fExplodeShellTime.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "ExplodeFormationId",  pair.Value.fExplodeFormationId.ToString() );
            StringManager.CreateNode(xmlDoc, nodeThorn, "SkillId", pair.Value.nSkillId.ToString());

        }
	}

	Dictionary<string, string> dicTemp = new Dictionary<string, string> ();
	public void GenerateThorn(XmlNode node)
	{
		if (node == null) {
			return;
		}

        string szVal = "";

		for (int i = 0; i < node.ChildNodes.Count; i++) {
			XmlNode sub_node = node.ChildNodes [i];
			dicTemp.Clear ();
			for (int j = 0; j < sub_node.ChildNodes.Count; j++) {
				dicTemp[sub_node.ChildNodes[j].Name] = sub_node.ChildNodes[j].InnerText;
			} //  end j
			string szId = dicTemp["ID"];
            int nType = 0;
            string szType = "";
            if ( !dicTemp.TryGetValue("Type", out szType) )
            {
                nType = int.Parse(szType);
            }
            sThornConfig config = NewMonsterConfig(szId);
			config.szId = szId;
            config.nType = nType;
            config.szDesc = dicTemp["Desc"];
			config.fFoodSize = float.Parse (dicTemp["FoodSize"]);
			config.fSelfSize = float.Parse (dicTemp["SelfSize"]);
            if (dicTemp.TryGetValue("Money", out szVal))
            {
                config.fMoney = float.Parse(szVal   );
            }
            config.szColor = dicTemp["Color"];
			config.fExp = float.Parse (dicTemp["Exp"]);
			config.fBuffId = float.Parse (dicTemp["BuffId"]);
			config.fExplodeChildNum = float.Parse (dicTemp["ExplodeChildNum"]);
			config.fExplodeMotherLeftPercent = float.Parse (dicTemp["ExplodeMotherLeftPercent"]);
			config.fExplodeRunDistance = float.Parse (dicTemp["ExplodeRunDistance"]);
			config.fExplodeRunTime = float.Parse (dicTemp["ExplodeRunTime"]);
			config.fExplodeStayTime = float.Parse (dicTemp["ExplodeStayTime"]);
			config.fExplodeShellTime = float.Parse (dicTemp["ExplodeShellTime"]);
			config.fExplodeFormationId = float.Parse (dicTemp["ExplodeFormationId"]);

            int nSkillId = CSkillSystem.SELECTABLE_SKILL_START_ID;
            string szSkillId = "";
            if ( dicTemp.TryGetValue("SkillId", out szSkillId))
            {
                if ( int.TryParse(szSkillId, out nSkillId) )
                {

                }
            }
            config.nSkillId = nSkillId;


            m_dicMonsterConfig[szId] = config;

        } // end i
		OnDropdownValueChanged_ThorszId();

    }


    public const float BEAN_COLLECTION_WORLD_SIZE = 122.88f;
    public const float BEAN_COLLECTION_WORLD_SIZE_HALF = BEAN_COLLECTION_WORLD_SIZE / 2f;
    public const float WORLD_POS_TO_TEX_POS_K = BEAN_COLLECTION_TEX_SIZE / BEAN_COLLECTION_WORLD_SIZE;
    public const int BEAN_COLLECTION_TEX_SIZE = 512;
    public const int BEAN_PIC_TEX_SIZE = 4;
    public GameObject m_goBeanContainer;
    public Sprite m_sprBeanPic;
    public Sprite m_sprBeanDeadPic;
    public Sprite m_sprBeanCollection;
    public GameObject m_preBeanCollection;
    public Color[] m_ColorBeanDead;
    public Color[] m_ColorBeanLive;
    public Color[] m_ColorAllEmpty;
    List<CBeanCollection> m_lstBeanCollection = new List<CBeanCollection>();

    int m_nCollectionGuid = 0;

    List<Vector2[]> m_lstLocalPos = new List<Vector2[]>();
    List<Vector2[]> m_lstTexPos = new List<Vector2[]>();

    public Vector2[] GetLocalPosArray( int nIdx )
    {
        if (nIdx >= m_lstLocalPos.Count)
        {
            Debug.LogError("if (nIdx >= m_lstLocalPos.Count)");
            nIdx = 0;
        }
        return m_lstLocalPos[nIdx];
    }

    public Vector2[] GetTexPosArray(int nIdx)
    {
        if (nIdx >= m_lstTexPos.Count)
        {
            Debug.LogError("if (nIdx >= m_lstTexPos.Count)");
            nIdx = 0;
        }
        return m_lstTexPos[nIdx];
    }

    public Sprite m_sprBeanCollectionEmpty;

    Dictionary<string, Color[]> m_dicBeanColor = new Dictionary<string, Color[]>();
    Color[] GenerateBeanColors( string szColor )
    {
        szColor = "#" + szColor;
        Color[] colors;
        if (m_dicBeanColor.TryGetValue(szColor, out colors))
        {
            return colors;
        }
        Color color = CColorManager.s_Instance.GetColorByString(szColor);
        colors = new Color[m_ColorBeanLive.Length];
        for (int i = 0; i < colors.Length; i++)
        {
            colors[i] = m_ColorBeanLive[i] * color;
        }
        m_dicBeanColor[szColor] = colors;
        return colors;
    }


    Dictionary<string, CBeanCollection> m_dicBeanCollections = new Dictionary<string, CBeanCollection>();
    Dictionary<int, CBeanCollection> m_dicBeanCollectionsByGuid = new Dictionary<int, CBeanCollection>();
    public CBeanCollection GetBeanCollectionByKey( string szKey )
    {
        CBeanCollection colle;
        if ( !m_dicBeanCollections.TryGetValue(szKey, out colle))
        {
            Debug.LogError( "bean colection not found by key" );
        }
        return colle;

    }

    int m_nCollectionIndex = 0;
    int m_nQuKuai = 0;
    int m_nQuKuaiIndex_i = 0;
    int m_nQuKuaiIndex_j = 0;
    bool m_bInitQuKuaiCompleted = false;
    int m_nTotalNUm = 0;
    bool m_bInitQuKuaiStarted = false;
    public void InitingBeanCollection()
    {
        return;

        m_ColorAllEmpty = m_sprBeanCollectionEmpty.texture.GetPixels();
        m_ColorBeanLive = m_sprBeanPic.texture.GetPixels();
        m_ColorBeanDead = m_sprBeanDeadPic.texture.GetPixels();
        int nTotalNUm = 0;
        float fWorldSize = MapEditor.s_Instance.GetWorldSize();
        m_nQuKuai = (int)( fWorldSize / BEAN_COLLECTION_WORLD_SIZE );
        if (m_nQuKuai % 2 != 0 )
        {
            m_nQuKuai++;
        }
        m_nQuKuai /= 2;
        m_nQuKuaiIndex_i = -m_nQuKuai;
        m_nQuKuaiIndex_j = -m_nQuKuai;
        /*
        for ( int i = -m_nQuKuai; i <= m_nQuKuai; i++ )
        {
            for (int j = -m_nQuKuai; j <= m_nQuKuai; j++)
            {
                CBeanCollection bean_colle = GameObject.Instantiate(m_preBeanCollection).GetComponent<CBeanCollection>();
                bean_colle.transform.SetParent(m_goBeanContainer.transform);
                string szKey = i + "_" + j;
                bean_colle.SetCollecionGuid(m_nCollectionIndex);
                m_dicBeanCollections[szKey] = bean_colle;
                m_dicBeanCollectionsByGuid[m_nCollectionIndex] = bean_colle;
                m_nCollectionIndex++;
                bean_colle.SetCollecionGuid(szKey);
                vecTempPos.x = i * BEAN_COLLECTION_WORLD_SIZE;
                vecTempPos.y = j * BEAN_COLLECTION_WORLD_SIZE;
                vecTempPos.z = 0f;
                //bean_colle.transform.localPosition = vecTempPos;
                bean_colle.SetPos(vecTempPos);
                nTotalNUm++;
            } // end j
        } // end i
        */
        

        m_bInitQuKuaiStarted = true;
    }

    void InitingBeanCollection_Loop()
    {
        if (!m_bInitQuKuaiStarted)
        {
            return;
        }

        if ( m_bInitQuKuaiCompleted )
        {
            return;
        }

        CBeanCollection bean_colle = GameObject.Instantiate(m_preBeanCollection).GetComponent<CBeanCollection>();
        bean_colle.transform.SetParent(m_goBeanContainer.transform);
        string szKey = m_nQuKuaiIndex_i + "_" + m_nQuKuaiIndex_j;
        bean_colle.SetCollecionGuid(m_nCollectionIndex);
        m_dicBeanCollections[szKey] = bean_colle;
        m_dicBeanCollectionsByGuid[m_nCollectionIndex] = bean_colle;
        m_nCollectionIndex++;
        bean_colle.SetCollecionGuid(szKey);
        vecTempPos.x = m_nQuKuaiIndex_i * BEAN_COLLECTION_WORLD_SIZE;
        vecTempPos.y = m_nQuKuaiIndex_j * BEAN_COLLECTION_WORLD_SIZE;
        vecTempPos.z = 0f;
        //bean_colle.transform.localPosition = vecTempPos;
        bean_colle.SetPos(vecTempPos);
        m_nTotalNUm++;

        if ( m_nQuKuaiIndex_j <= m_nQuKuai )
        {
            m_nQuKuaiIndex_j++;
        }
        else
        {
            if (m_nQuKuaiIndex_i <= m_nQuKuai)
            {
                m_nQuKuaiIndex_j = -m_nQuKuai;
                m_nQuKuaiIndex_i++;
            }
            else
            {
                m_bInitQuKuaiCompleted = true;
                vecTempPos.x = -BEAN_COLLECTION_WORLD_SIZE_HALF;
                vecTempPos.y = BEAN_COLLECTION_WORLD_SIZE_HALF;
                vecTempPos.z = 0;
                m_goBeanContainer.transform.localPosition = vecTempPos;
                Debug.Log( "区块的个数：" + m_nTotalNUm);
            }
        }
    }

    float m_fBeanInteractBallTimeCount = 0;

    void BeanInteractBallLoop()
    {
        /*
        for ( int i = 0; i < m_lstBeanCollection.Count; i++ )
        {
            CBeanCollection bc = m_lstBeanCollection[i];
            bc.DoBeanInteractBall();
        }
        */
    }


    public void RemoveBean( int nCollectionId, int nBeanIdx )
    {
        CBeanCollection colle;
        if( !m_dicBeanCollectionsByGuid.TryGetValue(nCollectionId, out colle) )
        {
            Debug.LogError("!m_dicBeanCollectionsByGuid.TryGetValue(nCollectionId, out colle)");
            return;
        }
        colle.RemoveBean(nBeanIdx);

        CBeanCollection.sBeanRebornInfo info;
        info.collection_guid = (ushort)nCollectionId;
        info.bean_idx = (ushort)nBeanIdx;
        info.fDeadTime = Main.GetTime();
        info.fRebornInterval = colle.GetBeanClassConfig(nBeanIdx).fRebornTime;
        AddToRebornQueue( info );
    }

    public void RebornBean(int nCollectionId, int nBeanIdx)
    {
        CBeanCollection collecion;
        if ( !m_dicBeanCollectionsByGuid.TryGetValue(nCollectionId, out collecion) )
        {
            Debug.LogError("if ( !m_dicBeanCollectionsByGuid.TryGetValue(nCollectionId, out collecion) )");
            return;
        }
           
        collecion.RebornBean(nBeanIdx);

    }

    List<CBeanCollection.sBeanRebornInfo> m_lstBeanRebornList = new List<CBeanCollection.sBeanRebornInfo>();
    void AddToRebornQueue(CBeanCollection.sBeanRebornInfo info)
    {
        m_lstBeanRebornList.Add( info );
    }

    float m_fBeanRebornLoopTimeCount = 0f;
    void BeanRebornLoop()
    {
        m_fBeanRebornLoopTimeCount += Time.deltaTime;
        if (m_fBeanRebornLoopTimeCount < 1f)
        {
            return;
        }
        m_fBeanRebornLoopTimeCount = 0f;
        for ( int i = m_lstBeanRebornList.Count - 1; i >= 0; i-- )
        {
            if ( Main.GetTime() - m_lstBeanRebornList[i].fDeadTime > 20f )
            {
                RebornBean(m_lstBeanRebornList[i].collection_guid, m_lstBeanRebornList[i].bean_idx);
                m_lstBeanRebornList.RemoveAt(i);
            }
        }
    }

    List<CBeanCollection> m_lstInteractingCollection = new List<CBeanCollection>();
    public void AddCollection( CBeanCollection collection )
    {
        m_lstInteractingCollection.Add(collection);
    }

    public void RemoveCollection(CBeanCollection collection)
    {
        m_lstInteractingCollection.Remove(collection);
    }

    public void GenerateBeansOfOneId( ref sThornConfig thorn_buildin_config, ref CClassEditor.sThornOfThisClassConfig thorn_in_class_config, ref string[] aryPos, int nIndex )
    {
       //for ( int i = 0; i < aryPos.Length; i+=2 )
       // {
            vecTempPos.x = float.Parse(aryPos[nIndex]);
            vecTempPos.y = float.Parse(aryPos[nIndex + 1]);
            vecTempPos.z = 0f;
            int idx_x = (int)( ( vecTempPos.x + (vecTempPos.x > 0? BEAN_COLLECTION_WORLD_SIZE_HALF : -BEAN_COLLECTION_WORLD_SIZE_HALF ) ) / BEAN_COLLECTION_WORLD_SIZE );
            int idx_y = (int)( ( vecTempPos.y + (vecTempPos.y > 0? BEAN_COLLECTION_WORLD_SIZE_HALF : -BEAN_COLLECTION_WORLD_SIZE_HALF ) ) / BEAN_COLLECTION_WORLD_SIZE );
            string szCollectionKey = idx_x + "_" + idx_y;
            CBeanCollection colle = GetBeanCollectionByKey(szCollectionKey);
            float fLocalPosX = vecTempPos.x - colle.GetLeftTopPos().x;
            float fLocalPosY = vecTempPos.y - colle.GetLeftTopPos().y;
            int nTexPosX = (int)(fLocalPosX * WORLD_POS_TO_TEX_POS_K );
            int nTexPosY = (int)(-fLocalPosY * WORLD_POS_TO_TEX_POS_K );
            Color[] colors = GenerateBeanColors(thorn_buildin_config.szColor);
            colle.GenerateBean(nTexPosX, nTexPosY, ref colors, fLocalPosX, fLocalPosY, ref thorn_buildin_config, ref thorn_in_class_config);
            CClassEditor.s_nTotalBeanNum++;
       // } // end i
    }

    public const int GENERATE_BEAN_NUM_PER_FRAME = 2;
    public struct sGenerateBeanNode
    {
        public sThornConfig thorn_buildin_config;
        public CClassEditor.sThornOfThisClassConfig thorn_in_class_config;
        public string[] aryPos;
        public int nCurNum;
    };
    List<sGenerateBeanNode> m_lstGenerateBeanNodeList = new List<sGenerateBeanNode>();
    public void AddToGenerateListQueue( ref sThornConfig thorn_buildin_config, ref CClassEditor.sThornOfThisClassConfig thorn_in_class_config, ref string[] aryPos)
    {
        sGenerateBeanNode node = new sGenerateBeanNode();
        node.nCurNum = 0;
        node.thorn_buildin_config = thorn_buildin_config;
        node.thorn_in_class_config = thorn_in_class_config;
        node.aryPos = aryPos;
        m_lstGenerateBeanNodeList.Add( node );
    }

    void GeneratingBean()
    {
        if ( !m_bInitQuKuaiCompleted )
        {
            return;
        }

        if (m_lstGenerateBeanNodeList.Count == 0)
        {
            return;
        }

        for (int i = m_lstGenerateBeanNodeList.Count - 1; i >= 0; i-- ) // 一个node就是一种类型的豆子
        {
            sGenerateBeanNode node = m_lstGenerateBeanNodeList[i];
            bool bThisNodeEnd = false;
            int nEnd = node.nCurNum + GENERATE_BEAN_NUM_PER_FRAME * 2;
            for ( int j = node.nCurNum; j < nEnd; j += 2 ) // 注意"j+=2"
            {
                if ( j >= node.aryPos.Length )
                {
                    bThisNodeEnd = true;
                    break; // exit for j
                }
                GenerateBeansOfOneId(ref node.thorn_buildin_config, ref node.thorn_in_class_config, ref node.aryPos, j);
                node.nCurNum = j + 2;
            } // end for j

            if (bThisNodeEnd)
            {
                m_lstGenerateBeanNodeList.RemoveAt(i);
                continue;
            }
            m_lstGenerateBeanNodeList[i] = node;


        } // end for i
    }
}

