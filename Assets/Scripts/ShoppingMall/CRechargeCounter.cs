﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CRechargeCounter : MonoBehaviour {

    public int m_nPrice;
    public ShoppingMallManager.eMoneyType m_eMonryType;
    public uint m_nGainMoneyNum;
    string m_szChargeProductId = "";

    public Text _txtGainMoneyNum;

	// Use this for initialization
	void Start () {
        _txtGainMoneyNum.text = m_nGainMoneyNum.ToString();

    }
	
	// Update is called once per frame
	void Update () {
      
            
	}

    public void OnClickButton_Buy()
    {
        //ShoppingMallManager.s_Instance.AddMoney(m_eMonryType, m_nGainMoneyNum);
        ShoppingMallManager.s_Instance._RechargeConfirmUI.gameObject.SetActive( true );
        ShoppingMallManager.s_Instance._RechargeConfirmUI.SetCostType( "即将消耗人民币：" );
        ShoppingMallManager.s_Instance._RechargeConfirmUI.SetCostNum(m_nPrice.ToString());
        ShoppingMallManager.s_Instance._RechargeConfirmUI.SetGain( "购买 " + m_nGainMoneyNum + " 咩币" );
        ShoppingMallManager.s_Instance._RechargeConfirmUI.m_nGainNum = m_nGainMoneyNum;
        ShoppingMallManager.s_Instance._RechargeConfirmUI.SetProductId( GetChargeProductId() );
    }

    public void SetGainMoneyNum( uint nNum )
    {
        m_nGainMoneyNum = nNum;
        _txtGainMoneyNum.text = m_nGainMoneyNum.ToString();
    }

    public void SetChargeProductId( string szChargeProductId)
    {
        m_szChargeProductId = szChargeProductId;
    }

    public string GetChargeProductId()
    {
        return m_szChargeProductId;
    }
}
