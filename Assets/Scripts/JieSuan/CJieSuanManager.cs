﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CJieSuanManager : MonoBehaviour {

    public static CJieSuanManager s_Instance;

    public GameObject _panelJieSuan;
    public GameObject _panelGeRenJieSuan;

    public Vector2[] m_aryCounterPos;
    int m_nMoveStatus = -1;
    int m_nQuanChangZuiJiaStatus = 0;
    public float m_fCounterMoveSpeed = 1000f;
    public float m_fZuiJiaKuangScaleSpeed = 1000f;

    public CJieSuanCounter[] m_aryJiesuanCounter;
    public Image _imgZuiJiaKuang;

    Vector3 m_vecDestPos = new Vector3();
    static Vector2 vecTempDir = new Vector2();
    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public const int MAX_COUNTER_NUM = 5;
    public const int CHAMPION_COUNTER_INDEX = 4;

    public GameObject _goCounterContainer;

    public CFrameAnimationEffect _effectQuanChangZuiJia_QianZou;
    public CFrameAnimationEffect _effectQuanChangZuiJia_ChiXu;

    public float m_fZhengTiJieSuanShowTime = 5f;
    float m_fZhengTiJieSuanTimeElapse = 0f;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        // Moving();
        // Scaling();

        /*
        if (m_nQuanChangZuiJiaStatus == 1)
        {
            if ( _effectQuanChangZuiJia_QianZou.isEnd() )
            {
                _effectQuanChangZuiJia_QianZou.gameObject.SetActive( false );
                _effectQuanChangZuiJia_ChiXu.gameObject.SetActive( true );
                _effectQuanChangZuiJia_ChiXu.BeginPlay( true );
                m_nQuanChangZuiJiaStatus = 2;
            }
        }
        */
        TapToExit();
        TapToExit_ZhengTiJieSuan();
    }

    public void Show()
    {
        CPaiHangBang_Mobile.s_Instance.RefreshPaiHangbangPlayerInfo();
        _panelJieSuan.SetActive(true);
        m_nMoveStatus = 0;
        UpdateJiesuanInfo_ByVolume();

        m_bShowingZhengTiJieSuan = true;
        if (UIManager.s_Instance._etcJoyStick)
        {
            UIManager.s_Instance._etcJoyStick.gameObject.SetActive(false);
        }

        m_bZhengTiJieSuan = true;
    }

    public float[] m_aryScalingSpeed;
    public float[] m_aryDestValue;

    void Moving()
    {
        /*
        if (m_nMoveStatus < 0 || m_nMoveStatus > 4)
        {
            return;
        }

        CJieSuanCounter counter = m_aryJiesuanCounter[m_nMoveStatus];
        m_vecDestPos = m_aryCounterPos[m_nMoveStatus];
        if ( Vector2.Distance(counter.transform.localPosition, m_vecDestPos) < 30 )
        {
            counter.transform.localPosition = m_vecDestPos;
            m_nMoveStatus++;
        }

        vecTempDir = m_vecDestPos - counter.transform.localPosition;
        vecTempDir.Normalize();
        vecTempPos = counter.transform.localPosition;
        vecTempPos.x += vecTempDir.x * m_fCounterMoveSpeed * Time.deltaTime;
        vecTempPos.y += vecTempDir.y * m_fCounterMoveSpeed * Time.deltaTime;
        counter.transform.localPosition = vecTempPos;
        */

        /*
        if (m_nMoveStatus == 1)
        {
            vecTempPos = _goCounterContainer.transform.localPosition;
            vecTempPos.x += m_fCounterMoveSpeed * Time.deltaTime;
            if (vecTempPos.x >= 0)
            {
                vecTempPos.x = 0;
                m_nMoveStatus = 2;


                _effectQuanChangZuiJia_QianZou.gameObject.SetActive( true );
                _effectQuanChangZuiJia_QianZou.BeginPlay( false );
                m_nQuanChangZuiJiaStatus = 1;
            }
            _goCounterContainer.transform.localPosition = vecTempPos;
        }
        */

        bool bEnd = false;
        if (m_nMoveStatus == 0)
        {
            for (int i = 0; i < m_aryJiesuanCounter.Length; i++)
            {
                CJieSuanCounter counter = m_aryJiesuanCounter[i];
                vecTempScale = counter.transform.localScale;
                vecTempScale.y += Time.deltaTime * m_aryScalingSpeed[m_nMoveStatus];
                counter.transform.localScale = vecTempScale;
                if (vecTempScale.y >= m_aryDestValue[m_nMoveStatus])
                {
                    vecTempScale.y = m_aryDestValue[m_nMoveStatus];
                    counter.transform.localScale = vecTempScale;
                    bEnd = true;
                }
            }


            if (bEnd)
            {
                m_nMoveStatus = 1;
                return;

            }
        }
        else if (m_nMoveStatus == 1)
        {
            for (int i = 0; i < m_aryJiesuanCounter.Length; i++)
            {
                CJieSuanCounter counter = m_aryJiesuanCounter[i];
                vecTempScale = counter.transform.localScale;
                vecTempScale.x += Time.deltaTime * m_aryScalingSpeed[m_nMoveStatus];
                counter.transform.localScale = vecTempScale;
                if (vecTempScale.x >= m_aryDestValue[m_nMoveStatus])
                {
                    bEnd = true;
                }
            }


            if (bEnd)
            {
                m_nMoveStatus = 2;
                return;

            }
        }
        else if (m_nMoveStatus == 2)
        {
            for (int i = 0; i < m_aryJiesuanCounter.Length; i++)
            {
                CJieSuanCounter counter = m_aryJiesuanCounter[i];
                vecTempScale = counter.transform.localScale;
                vecTempScale.x -= Time.deltaTime * m_aryScalingSpeed[m_nMoveStatus];
                counter.transform.localScale = vecTempScale;
                if (vecTempScale.x <= m_aryDestValue[m_nMoveStatus])
                {
                    bEnd = true;
                }


            }

            if (bEnd)
            {
                m_nMoveStatus = 3;
                return;
            }
        }
        else if (m_nMoveStatus == 3)
        {
            for (int i = 0; i < m_aryJiesuanCounter.Length; i++)
            {
                CJieSuanCounter counter = m_aryJiesuanCounter[i];
                vecTempScale = counter.transform.localScale;
                vecTempScale.x += Time.deltaTime * m_aryScalingSpeed[m_nMoveStatus];
                counter.transform.localScale = vecTempScale;
                if (vecTempScale.x >= m_aryDestValue[m_nMoveStatus])
                {
                    bEnd = true;
                }

            }


            if (bEnd)
            {
                m_nMoveStatus = 4;
                return;
            }
        }
        else if (m_nMoveStatus == 4)
        {
            for (int i = 0; i < m_aryJiesuanCounter.Length; i++)
            {
                CJieSuanCounter counter = m_aryJiesuanCounter[i];
                vecTempScale = counter.transform.localScale;
                vecTempScale.x -= Time.deltaTime * m_aryScalingSpeed[m_nMoveStatus];
                counter.transform.localScale = vecTempScale;
                if (vecTempScale.x <= m_aryDestValue[m_nMoveStatus])
                {
                    vecTempScale.x = m_aryDestValue[m_nMoveStatus];
                    counter.transform.localScale = vecTempScale;
                    bEnd = true;
                }


            }

            if (bEnd)
            {
                m_nMoveStatus = 5;
                return;
            }
        }
        else if (m_nMoveStatus == 5)
        {

                CJieSuanCounter counter = m_aryJiesuanCounter[0];
                vecTempScale = counter.transform.localScale;
                float fDelta = Time.deltaTime * m_aryScalingSpeed[m_nMoveStatus];
                vecTempScale.x += fDelta;
                vecTempScale.y += fDelta;
                counter.transform.localScale = vecTempScale;
                if (vecTempScale.x >= m_aryDestValue[m_nMoveStatus] && vecTempScale.y >= m_aryDestValue[m_nMoveStatus])
                {
                    vecTempScale.x = m_aryDestValue[m_nMoveStatus];
                    vecTempScale.y = m_aryDestValue[m_nMoveStatus];
                    counter.transform.localScale = vecTempScale;
                    bEnd = true;
                }



            if (bEnd)
            {
                m_nMoveStatus = 6;
                return;
            }
        }
        else if (m_nMoveStatus == 6)
        {
     
                CJieSuanCounter counter = m_aryJiesuanCounter[0];
                vecTempScale = counter.transform.localScale;
                float fDelta = Time.deltaTime * m_aryScalingSpeed[m_nMoveStatus];
                vecTempScale.x -= fDelta;
                vecTempScale.y -= fDelta;
                counter.transform.localScale = vecTempScale;
                if (vecTempScale.x <= m_aryDestValue[m_nMoveStatus] && vecTempScale.y <= m_aryDestValue[m_nMoveStatus])
                {
                    vecTempScale.x = m_aryDestValue[m_nMoveStatus];
                    vecTempScale.y = m_aryDestValue[m_nMoveStatus];
                    counter.transform.localScale = vecTempScale;
                    bEnd = true;
                }

                if (bEnd)
                {
                    m_nMoveStatus = 7;

                    counter.ShowZuiJiaKuang();

                _effectQuanChangZuiJia_ChiXu.gameObject.SetActive( true );
                _effectQuanChangZuiJia_ChiXu.BeginPlay( false );

                return;
                }
        }

    }

    public void Like( int nCounterId, bool bIsMainPlayer )
    {
        m_aryJiesuanCounter[nCounterId].AddOneLike();

        if (bIsMainPlayer)
        {
            for (int i = 0; i < m_aryJiesuanCounter.Length; i++)
            {
                CJieSuanCounter counter = m_aryJiesuanCounter[i];
                counter._skeleLike.gameObject.SetActive(false);
                if (i == nCounterId)
                {
                    counter._imgLiked_Click.gameObject.SetActive(true);
                    counter._imgLiked_NotClick.gameObject.SetActive(false);
                }
                else
                {
                    counter._imgLiked_Click.gameObject.SetActive(false);
                    counter._imgLiked_NotClick.gameObject.SetActive(true);
                }
            }
        }
    }

    void Scaling()
    {
        /*
        if (m_nMoveStatus != 5)
        {
            return;
        }

        vecTempScale = _imgZuiJiaKuang.transform.localScale;
        vecTempScale.x -= m_fZuiJiaKuangScaleSpeed * Time.deltaTime;
        vecTempScale.y -= m_fZuiJiaKuangScaleSpeed * Time.deltaTime;
        if (vecTempScale.x <= 1f || vecTempScale.y <= 1f )
        {
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            m_nMoveStatus++;
        }
        _imgZuiJiaKuang.transform.localScale = vecTempScale;
        */
    }

    void UpdateJiesuanInfo_ByVolume()
    {
        /*
        List<CPaiHangBang_Mobile.sPlayerAccount> lstPlayers = CPaiHangBang_Mobile.s_Instance.GetPaiHangBangPlayerList();
        CPaiHangBang_Mobile.sPlayerAccount champion = lstPlayers[0];
        CJieSuanCounter counterChampion = m_aryJiesuanCounter[CHAMPION_COUNTER_INDEX];
        counterChampion.UpdateInfo(champion);

        int nCounterIndex = CHAMPION_COUNTER_INDEX - 1;
        for ( int i = 1; i < lstPlayers.Count; i++ )
        {
            if (nCounterIndex < 0)
            {
                break;
            }
            CJieSuanCounter counter = m_aryJiesuanCounter[nCounterIndex];
            counter.UpdateInfo(lstPlayers[i]);
            nCounterIndex--;
        }
        */

        Dictionary<int, CPaiHangBang_Mobile.sPlayerAccount> dicPlayersInfo = CPaiHangBang_Mobile.s_Instance.GetPaiHangBangPlayerDic();
        int nPlayerId_MaxVolume = 0;
        float fMaxVolume = 0;
        int nPlayerId_MaxEatThornNum = 0;
        int nMaxEatThornNum = 0;
        int nPlayerId_MaxKillNum = 0;
        int nMaxKillNum = 0;
        bool bFirst = true;
        foreach (KeyValuePair<int, CPaiHangBang_Mobile.sPlayerAccount> pair in dicPlayersInfo)
        {
            if (bFirst )
            {
                nPlayerId_MaxVolume = pair.Key;
                nPlayerId_MaxEatThornNum = pair.Key;
                nPlayerId_MaxKillNum = pair.Key;
                fMaxVolume = pair.Value.fTotalVolume;
                nMaxEatThornNum = pair.Value.nEatThornNum;
                nMaxKillNum = pair.Value.nKillNum;
                bFirst = false;
                continue;
            }

            if (pair.Value.fTotalVolume > fMaxVolume)
            {
                fMaxVolume = pair.Value.fTotalVolume;
                nPlayerId_MaxVolume = pair.Key;
            }

            if ( pair.Value.nEatThornNum > nMaxEatThornNum)
            {
                nPlayerId_MaxEatThornNum = pair.Key;
                nMaxEatThornNum = pair.Value.nEatThornNum;
            }

            if (pair.Value.nKillNum > nMaxKillNum)
            {
                nPlayerId_MaxKillNum = pair.Key;
                nMaxKillNum = pair.Value.nKillNum;
            }
        } // end foreach

        CJieSuanCounter counter_champion = m_aryJiesuanCounter[0];
        CJieSuanCounter counter_max_kill = m_aryJiesuanCounter[1];
        CJieSuanCounter counter_max_eat_thorn = m_aryJiesuanCounter[2];

        counter_champion.UpdateInfo( dicPlayersInfo[nPlayerId_MaxVolume] );
        counter_max_kill.UpdateInfo(dicPlayersInfo[nPlayerId_MaxKillNum]);
       
        
        counter_max_eat_thorn.UpdateInfo(dicPlayersInfo[nPlayerId_MaxEatThornNum]);

    }

    void UpdateJiesuanInfo_ByJueShengQiu()
    {

    }

    public void OnClickButton_ShowGeRenJieSuan()
    {
        ShowGeRenJieSuan();
    }

    public void ShowGeRenJieSuan()
    {
        _panelJieSuan.SetActive(false);
        _panelGeRenJieSuan.SetActive(true);
        m_bGenRenJieSuan = true;
    }

    bool m_bGenRenJieSuan = false;
    void TapToExit()
    {
            if (!m_bGenRenJieSuan)
            {
                return;
            }

            if (Input.GetMouseButtonDown(0) && !UIManager.IsPointerOverUI())
            {
                Main.s_Instance.ExitGame();
            }
       
    }

    bool m_bZhengTiJieSuan = false;
    void TapToExit_ZhengTiJieSuan()
    {
        if ( !m_bZhengTiJieSuan)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0) && !UIManager.IsPointerOverUI())
        {

            ShowGeRenJieSuan();
        }
    }

    bool m_bShowingZhengTiJieSuan = false;
    void ZhengTiJieSuanLoop()
    {
        if ( !m_bShowingZhengTiJieSuan)
        {
            return;
        }

        m_fZhengTiJieSuanTimeElapse += Time.deltaTime;
        if (m_fZhengTiJieSuanTimeElapse >= m_fZhengTiJieSuanShowTime)
        {
            ShowGeRenJieSuan();
        }
    }
}
