﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CQueueItem : CCyberTreeListItem {

    public Image _imgAvatar;
    public Text _txtPlayerName;
    public Text _txtLevel;
    public Text _txtArea;
    public Text _txtPlayerId;

    int m_nPlayerId = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetPlayerId( int nPlayerId )
    {
        m_nPlayerId = nPlayerId;
        //_imgAvatar.sprite = CSkinManager.LoadSkinById(nPlayerId);
    }

    public void SetSkinId( int nPlayerId, int nSkinId )
    {
        _imgAvatar.sprite = AccountData.s_Instance.GetSpriteByItemId(nSkinId) ;// CSkinManager.LoadSkinById(nSkinId);   
        if (nSkinId == AccountData.INVALID_SKIN_ID)
        {
            _imgAvatar.color = ResourceManager.GetPlayerColorById(nPlayerId + 1);
            _imgAvatar.sprite = AccountData.s_Instance.m_sprNoSkin;
        }
        else
        {
            _imgAvatar.color = Color.white;
        }
        _imgAvatar.gameObject.SetActive( true );
    }

    public void SetPlayerName( string szPlayerName )
    {
        _txtPlayerName.text = szPlayerName;
    }
}
