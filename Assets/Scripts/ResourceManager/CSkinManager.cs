﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class CSkinManager : MonoBehaviour {

    static Dictionary<int, Sprite> m_dicSkinSprites = new Dictionary<int, Sprite>();
    static Dictionary<int, Material> m_dicSkinMaterial = new Dictionary<int, Material>();

    public const int MAX_SKIN_NUM = 41;

    static Material m_matNoSkinMaterial;

    // Use this for initialization
    void Start () {
        InitSkinTextureAndMaterial();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void  InitSkinTextureAndMaterial()
    {
        for ( int i = 0; i <= MAX_SKIN_NUM; i++ )
        {
            LoadSkinById( i );
        }

        Shader sha = Shader.Find("Custom/DefaultSkinShader");
        foreach( KeyValuePair<int, Sprite> pair in m_dicSkinSprites )
        {
            Material mat = new Material(sha);
            mat.mainTexture = LoadSkinById(pair.Key).texture;// _arySkinTextures[i].texture;
            m_dicSkinMaterial[pair.Key] = mat;
        }
    }

    public static Material GetMaterialByPlayerId( int nPlayerId )
    {
        return m_dicSkinMaterial[nPlayerId];
    }

    public static Sprite LoadSkinById( int nPlayerId )
    {
        if (ResourceManager.s_Instance == null)
        {
            return null;
        }
        return ResourceManager.s_Instance.GetSkinSprite(nPlayerId);
        /*
        Sprite sprite = null;
        if ( m_dicSkinSprites.TryGetValue(nPlayerId, out sprite) )
        {
            return sprite;
        }

    
        string szFileName = "Skins/" + nPlayerId + ".png";

        //创建文件流
        FileStream fileStream = new FileStream(szFileName, FileMode.Open, FileAccess.Read);
        fileStream.Seek(0, SeekOrigin.Begin);
        //创建文件长度的缓冲区
        byte[] bytes = new byte[fileStream.Length];
        //读取文件
        fileStream.Read(bytes, 0, (int)fileStream.Length);
        //释放文件读取liu
        fileStream.Close();
        fileStream.Dispose();
        fileStream = null;

        //创建Texture
        int width = 128;
        int height = 128;
        Texture2D texture2D = new Texture2D(width, height);
        texture2D.LoadImage(bytes);

        sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height),
            new Vector2(0.5f, 0.5f));
        m_dicSkinSprites[nPlayerId] = sprite;

        return sprite;
        */
    }

   
   
}
