﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
public class CTiaoZi : MonoBehaviour {

    public SortingGroup _sortingGroup;

    static Vector3 vecTempScale = new Vector3();
    static Vector3 vecTempPos = new Vector3();
    static Color vecTempColor = new Color();

    public TextMesh _tmMain;
    public CanvasGroup _canvasGroup;
    public SpriteRenderer _srBg;
    public SpriteRenderer _srTitle;

    public Text[] _txtContent;
	public TextMesh[] _txtMeshContent;
	public TextMesh[] _txtMeshContent_MiaoBian;

    CTiaoZiManager.eTiaoZiType m_eType = CTiaoZiManager.eTiaoZiType.none;

    public Ball m_Ball = null;

    bool m_bActive = false;

    int m_nStatus = 0;

    Vector2 m_vecParentV = new Vector2();

    /// <summary>
    ///  type1: 抛物线
    /// </summary>
    public float m_fPaoWuXianVx0 = 0f;
    public float m_fPaoWuXianVy0 = 0f;
    public float m_fPaoWuXianT = 0f;
    public float m_fPaoWuXianA = 0f;
    public float m_fPaoWuXianAlphaA = 0f;

    /// <summary>
    /// type2:原地变大，然后一边上升一边变小
    /// </summary>
    public float m_fBianDaMaxScale = 0f;
    public float m_fBianDaA = 0f;
    public float m_fSuoXiaoA = 0f;
    public float m_fShangShengT = 0f;
    public float m_fShangShengBanJingXiShu = 0f;
    public float m_fShangShengV0 = 0f;
    public float m_fShangShengA = 0f;
    public float m_fShangShengAlphaA = 0f;

    /// <summary>
    /// / type3: 暴击
    /// </summary>
    public float m_fType3_ScaleXiShu = 0f;
    public float m_fType3_BianDaA = 0f;
    public float m_fType3_BianDaT = 0f;
    public float m_fType3_ChiXuT = 0f;
    public float m_fType3_BianXiaoA = 0f;
    public float m_fType3_BianXiaoT = 0f;
    public float m_fType3_MaxScale = 0f;
    public float m_fType3_CurChiXuTime = 0f;


    // type4： 原地展示
    public float m_fType4_CurChiXuTime = 0f;
    public float m_fType4_ChiXuTime = 0f;


    /// <summary>
    /// / type: 湮灭
    /// </summary>
    public float m_fYanMie_MaxScale = 0f;
    public float m_fYanMie_BianXiaoEndScale = 0f;
    public float m_fYanMie_ChiXuT = 0f;
    public float m_fYanMie_XiaJiangDistance = 0f;

    public float m_fYanMie_BianDaA = 0f;
    public float m_fYanMie_BianXiaoA = 0f;
    public float m_fYanMie_BianXiaoAlphaA = 0f;
    public float m_fYanMie_XiaJiangA = 0f;
    public float m_fYanMie_XiaJiangV0 = 0f;
    public float m_fYanMie_XiaJiangV = 0f;
    public float m_fYanMie_CurChiXuTime = 0f;


    /// <summary>


    /// type： tunshi 
    public float m_fTunShi_XiaJiang_V = 0f;
    public float m_fTunShi_Aplha_V = 0f;
    public float m_fTunShi_Scale_V = 0f;
    public float m_fTunShi_ChiXu = 0f;

    public float m_fTunShi_CurChiXuTime = 0;


    


    Vector3 m_vecStartPos = new Vector3();
    Vector3 m_vecCurPos = new Vector3();
    float m_fCurAlpha = 1f;
    float m_fCurScale = 0f;

    // Use this for initialization
    void Start () {
		if (_txtMeshContent != null ) {
			for (int i = 0; i < _txtMeshContent.Length; i++) {
				if (_txtMeshContent [i]) {
					_txtMeshContent [i].GetComponent<Renderer> ().sortingOrder = 0;
				}
			}
		}

		if (_txtMeshContent_MiaoBian != null) {
			for (int i = 0; i < _txtMeshContent_MiaoBian.Length; i++) {
				if (_txtMeshContent_MiaoBian [i]) {
					_txtMeshContent_MiaoBian [i].GetComponent<Renderer> ().sortingOrder = 1;
				}
			}
		}
    }
	
	// Update is called once per frame
	void Update () {
        

    }

    private void FixedUpdate()
    {
        TiaoZiLoop(); 
    }

    public void SetParent( Ball ball )
    {
        m_Ball = ball;
    }

    public Color GetColor()
    {
        if (_tmMain)
        {
            return _tmMain.color;
        }
        return Color.white;
    }

    public void SetColor( Color color )
    {
        if (_tmMain)
        {
            _tmMain.color = color;
        }

  
    }

	float m_fBgAlpha =  1f;
	public void SetBgColor( Color color )
	{
		if (_srBg != null) {
			_srBg.color = color;
			m_fBgAlpha = color.a;
		}
	}

    public void SetTextAlign( int nIndex, TextAnchor align )
    {
		if (_txtContent == null) {
			return;
		}

        if (nIndex >= _txtContent.Length)
        {
            return;
        }
        if (_txtContent[nIndex] != null)
        {
            _txtContent[nIndex].alignment = align;
        }
    }

    public void SetTextColor(int nIndex, Color color )
    {
		
		if (_txtMeshContent == null) {
			return;
		}

		if (nIndex >= _txtMeshContent.Length)
        {
            return;
        }

		if (_txtMeshContent[nIndex] != null)
        {
			_txtMeshContent[nIndex].color = color;
        }
		
    }

	public void SetTitleSprVisible(bool bVisible)
	{
		if (_srTitle != null) {
			_srTitle.gameObject.SetActive (bVisible);
		}
	}

    public void SetText( int nIndex, string szContent )
    {
		if (_txtMeshContent == null) {
			return;
		}

		if ( nIndex >= _txtMeshContent.Length)
        {
            return;
        }

		if (_txtMeshContent[nIndex] != null)
        {
			_txtMeshContent[nIndex].text = szContent;
        }
		if (_txtMeshContent_MiaoBian[nIndex] != null)
		{
			_txtMeshContent_MiaoBian[nIndex].text = szContent;
		}
    }

    public void SetParentV( float fVX, float fVY )
    {
        m_vecParentV.x = fVX;
        m_vecParentV.y = fVY;
    }

    public void SetPos( Vector3 pos )
    {
        this.transform.position = pos;


    }

   

    public Vector3 GetPos()
    {
        return this.transform.position;
    }

    public void SetAlpha(float fAlpha)
    {
        if ( _srTitle )
        {
            vecTempColor = _srTitle.color;
            vecTempColor.a = fAlpha;
            _srTitle.color = vecTempColor;
        }

        if (_srBg)
        {
            vecTempColor = _srBg.color;
            vecTempColor.a = fAlpha;
			if (fAlpha > m_fBgAlpha) {
				vecTempColor.a = m_fBgAlpha;
			}
            _srBg.color = vecTempColor;
        }

        if (_srTitle)
        {
            vecTempColor = _srTitle.color;
            vecTempColor.a = fAlpha;
            _srTitle.color = vecTempColor;
        }

		if (_txtMeshContent != null) {
			for (int i = 0; i < _txtMeshContent.Length; i++) {
				if (_txtMeshContent [i] != null) {
					vecTempColor = _txtMeshContent [i].color;
					vecTempColor.a = fAlpha;
					_txtMeshContent [i].color = vecTempColor;
				}
			}
		}

		if (_txtMeshContent_MiaoBian != null) {
			for (int i = 0; i < _txtMeshContent_MiaoBian.Length; i++) {
				if (_txtMeshContent_MiaoBian [i] != null) {
					vecTempColor = _txtMeshContent_MiaoBian [i].color;
					vecTempColor.a = fAlpha;
					_txtMeshContent_MiaoBian [i].color = vecTempColor;
				}
			}
		}
    }

    public void SetScale( float fScale )
    {
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }

    public float GetScale()
    {
        return this.transform.localScale.x;
    }

    // type:吞噬
    public void SetParams_TunShi( float fTiaoZiScale, float fXiaJiangT, float fChiXuT, float fXiaJiangDistance )
    {
        m_fTunShi_XiaJiang_V = fXiaJiangDistance / fXiaJiangT;
        m_fTunShi_Aplha_V = -1 / fXiaJiangT;
        m_fTunShi_Scale_V = -0.5f * fTiaoZiScale / fXiaJiangT;
        m_fTunShi_ChiXu = fChiXuT;
        
    }

    // type: 湮灭
    public void SetParams_YanMie( float fTiaoZiScale, float fBianDaT, float fBianXiaoT, float fChiXuT, float fXiaJiangDistance)
    {
        m_fYanMie_XiaJiangA = CyberTreeMath.GetA(fXiaJiangDistance, fBianXiaoT);
        m_fYanMie_XiaJiangV0 = CyberTreeMath.GetV0(fXiaJiangDistance, fBianXiaoT);

        m_fYanMie_BianXiaoAlphaA = -1f / fBianXiaoT;

        m_fYanMie_MaxScale = fTiaoZiScale;
        m_fYanMie_BianDaA = m_fYanMie_MaxScale / fBianDaT;
        m_fYanMie_BianXiaoA = -m_fYanMie_BianDaA;

        m_fYanMie_ChiXuT = fChiXuT;

        m_fYanMie_BianXiaoEndScale = m_fYanMie_MaxScale * CTiaoZiManager.s_Instance.m_fYanMie_BianXiaoEndXiShu;

		SetAlpha (1f);
    }

    // type3: 暴击
    public void SetParams_Type3(float fSize, float fScaleXiShu, float fBianDaT, float BianXiaoT, float fChiXuT )
    {
        m_fType3_MaxScale = fScaleXiShu * fSize;
        m_fType3_BianDaA = m_fType3_MaxScale / fBianDaT;
        m_fType3_BianXiaoA = -m_fType3_MaxScale / BianXiaoT;
        m_fType3_ChiXuT = fChiXuT;
    }

    // type2: 先原地变大，再一边直线上升一边变小
    public void SetParams_Type2(float fSize, float fMaxScale)
    {
        m_fBianDaMaxScale = fMaxScale;// CTiaoZiManager.s_Instance.m_fBianDaBanJingXiShu * fSize;
        float s = fSize * CTiaoZiManager.s_Instance.m_fShangShengBanJingXiShu;
        float t = CTiaoZiManager.s_Instance.m_fShangShengT;
        m_fShangShengA = CyberTreeMath.GetA(s, t);
        m_fShangShengV0 = CyberTreeMath.GetV0(s, t);
        m_fBianDaA = m_fBianDaMaxScale / CTiaoZiManager.s_Instance.m_fBianDaT;
        m_fSuoXiaoA = -0.1f * m_fBianDaMaxScale / t;
        m_fShangShengAlphaA = CyberTreeMath.Get_A_By_V0_And_t(1f, t );
        SetScale(0);
    }

    // type1: 抛物线 
    public void SetParams_Type1(float fSize, float fBanJingXiShu = 0)
    {
        //this.SetScale(CTiaoZiManager.s_Instance.m_fPaoWuXianSizeXiShu * fSize);
        float s = 0f;
        if (fBanJingXiShu == 0)
        {
            s = fSize * CTiaoZiManager.s_Instance.m_fPaoWuXianBanJingXiShu;
        }
        else
        {
            s = fSize * fBanJingXiShu; 
        }

        

        float t = CTiaoZiManager.s_Instance.m_fPaoWuXianT;
        m_fPaoWuXianAlphaA = CyberTreeMath.Get_A_By_V0_And_t(1f, t / 2f );
        m_fPaoWuXianVy0 = CyberTreeMath.GetV0(s, t);
        m_fPaoWuXianA = CyberTreeMath.GetA(s, t);
        m_fPaoWuXianVx0 = CTiaoZiManager.s_Instance.m_fPaoWuXianVx0;
    }

    // type4： 原地展示
    public void SetParams_Type4( float fChiXuTime )
    {
        m_fType4_ChiXuTime = fChiXuTime;

        BeginTiaoZi(CTiaoZiManager.eTiaoZiType.type_4);
    }

    CTiaoZiManager.eTiaoZiType m_eZiYuanType = CTiaoZiManager.eTiaoZiType.jinbi;
    public void SetType(CTiaoZiManager.eTiaoZiType eType)
    {
        m_eZiYuanType = eType;
    }

    public CTiaoZiManager.eTiaoZiType GetType()
    {
        return m_eZiYuanType;
    }

	float m_fType1_CurChiXuTime = 0f;
    public void BeginTiaoZi( CTiaoZiManager.eTiaoZiType eType )
    {
        m_eType = eType;
        m_bActive = true;
        m_nStatus = 0;
        m_vecStartPos = GetPos();
        m_vecCurPos = m_vecStartPos;
        m_fCurAlpha = 1f;

        if (eType == CTiaoZiManager.eTiaoZiType.type_1)
        {
			m_fType1_CurChiXuTime = 0;
        }
        else if (eType == CTiaoZiManager.eTiaoZiType.type_2)
        {
            m_fCurScale = 0f;
           
        }
        else if (eType == CTiaoZiManager.eTiaoZiType.type_3)
        {
            m_fCurScale = 0f;
            m_fType3_CurChiXuTime = 0f;
            SetScale(m_fCurScale);
        }
        else if (eType == CTiaoZiManager.eTiaoZiType.type_4)
        {
            m_fType4_CurChiXuTime = 0f;
        }
        else if (eType == CTiaoZiManager.eTiaoZiType.yanmie)
        {
            m_fCurScale = 0f;
            m_fYanMie_CurChiXuTime = 0f;
            SetScale(m_fCurScale);
        }
        else if (eType == CTiaoZiManager.eTiaoZiType.tunshi)
        {
            m_fCurScale = 0f;
            m_fTunShi_CurChiXuTime = 0f;
        }

    }

    void TiaoZiLoop()
    {
        if ( !m_bActive)
        {
            return;
        }

        if (m_Ball != null)
        {
            vecTempPos = GetPos();
            vecTempPos.x += m_Ball.GetRealTimeSpeed() * m_Ball.GetDir().x * Time.fixedDeltaTime;
            vecTempPos.y += m_Ball.GetRealTimeSpeed() * m_Ball.GetDir().y * Time.fixedDeltaTime;
            SetPos(vecTempPos);
          
        }

        
        TiaoZiLoop_Type1();

        TiaoZiLoop_Type2();
        TiaoZiLoop_Type3();
        TiaoZiLoop_Type4();
        TiaoZiLoop_YanMie();
        TiaoZiLoop_TunShi();
		
    }

    // 吞噬Loop
    void TiaoZiLoop_TunShi()
    {
        if (m_eType != CTiaoZiManager.eTiaoZiType.tunshi)
        {
            return;
        }

        if (m_nStatus == 0) // 持续阶段
        {
            m_fTunShi_CurChiXuTime += Time.fixedDeltaTime;
            if (m_fTunShi_CurChiXuTime >= m_fTunShi_ChiXu)
            {
                m_fCurAlpha = 1f;
                m_nStatus = 1;
            }
        }
        else if (m_nStatus == 1) // 消失阶段
        {
            /*
            vecTempPos = GetPos();
            vecTempPos.y -= m_fTunShi_XiaJiang_V * Time.fixedDeltaTime;
            SetPos(vecTempPos);
            */
            m_fCurAlpha += m_fTunShi_Aplha_V * Time.fixedDeltaTime;
            /*
            float fCurScale = GetScale();
            fCurScale += m_fTunShi_Scale_V * Time.fixedDeltaTime;
            SetScale(fCurScale);
            */
            SetAlpha(m_fCurAlpha);

            if (m_fCurAlpha <= 0f)
            {
                EndTiaoZi_TunShi();
            }
        }
    }

    public void SetStatus( int nStatus )
    {
        m_nStatus = nStatus;
    }

    // 湮灭Loop
    void TiaoZiLoop_YanMie()
    {
        if (m_eType != CTiaoZiManager.eTiaoZiType.yanmie)
        {
            return;
        }

        if (m_nStatus == 0) // 变大阶段
        {


            // 缩放
            m_fCurScale += m_fYanMie_BianDaA * Time.fixedDeltaTime;
            SetScale(m_fCurScale);




            if (m_fCurScale >= m_fYanMie_MaxScale)
            {
                m_fCurScale = m_fYanMie_MaxScale;
                SetScale(m_fCurScale);
                m_nStatus = 1;
            }
        }
        else if (m_nStatus == 1) // 等待阶段
        {
            m_fYanMie_CurChiXuTime += Time.fixedDeltaTime;
            if (m_fYanMie_CurChiXuTime >= m_fYanMie_ChiXuT)
            {
                m_fYanMie_CurChiXuTime = 0;
                m_fYanMie_XiaJiangV = m_fYanMie_XiaJiangV0;
                m_fCurAlpha = 1f;
                m_nStatus = 2;
            }
        }
        else if (m_nStatus == 2) // 渐隐阶段
        {
            /*
            // 缩放
            m_fCurScale += m_fYanMie_BianXiaoA * Time.fixedDeltaTime;
            SetScale(m_fCurScale);

            // 下降
            m_fYanMie_XiaJiangV += m_fYanMie_XiaJiangA * Time.fixedDeltaTime;
            vecTempPos = GetPos();
            vecTempPos.y -= m_fYanMie_XiaJiangV * Time.fixedDeltaTime;
            SetPos(vecTempPos);
            */

            // 渐隐
            m_fCurAlpha += m_fYanMie_BianXiaoAlphaA * Time.fixedDeltaTime;
            SetAlpha(m_fCurAlpha);
            

            if (m_fCurAlpha <= 0)
            {
                EndTiaoZi_YanMie();
            }
        }
    }


    void TiaoZiLoop_Type3()
    {
        if (m_eType != CTiaoZiManager.eTiaoZiType.type_3)
        {
            return;
        }


        if (m_nStatus == 0) // 变大阶段
        {
            m_fCurScale += m_fType3_BianDaA * Time.fixedDeltaTime;
            SetScale(m_fCurScale);

            if (m_fCurScale >= m_fType3_MaxScale)
            {
                m_fCurScale = m_fType3_MaxScale;
                m_nStatus = 1;
            }
        }
        else if (m_nStatus == 1) // 持续阶段
        {
            m_fType3_CurChiXuTime += Time.fixedDeltaTime;
            if (m_fType3_CurChiXuTime >= m_fType3_ChiXuT)
            {
                m_nStatus = 2;
            }
        }
        else if (m_nStatus == 2) // 变小阶段
        {
            // 缩放
            m_fCurScale += m_fType3_BianXiaoA * Time.fixedDeltaTime;
            SetScale(m_fCurScale);

            if (m_fCurScale <= 0)
            {
                EndTiaoZi_Type3();
            }
        }



    }

    void TiaoZiLoop_Type2()
    {
        if (m_eType != CTiaoZiManager.eTiaoZiType.type_2)
        {
            return;
        }

        if (m_nStatus == 0) // 原地膨胀阶段
        {
            m_fCurScale += m_fBianDaA * Time.fixedDeltaTime;
            SetScale(m_fCurScale);

            if (m_fCurScale >= m_fBianDaMaxScale)
            {
                m_fCurScale = m_fBianDaMaxScale;
                m_nStatus = 1;
            }
        }
        else if (m_nStatus == 1) // 上升阶段
        {
            // 位置
            vecTempPos = GetPos();
            vecTempPos.y += m_fShangShengV0 * Time.fixedDeltaTime;
            m_fShangShengV0 += m_fShangShengA * Time.fixedDeltaTime;
            SetPos(vecTempPos);

            // 透明度
            m_fCurAlpha += m_fShangShengAlphaA * Time.fixedDeltaTime;
            vecTempColor = GetColor();
            vecTempColor.a = m_fCurAlpha;
            SetColor(vecTempColor);

            // 缩放
            m_fCurScale += m_fSuoXiaoA * Time.fixedDeltaTime;
            SetScale(m_fCurScale);



            if (m_fShangShengV0 <= 0)
            {
                EndTiaoZi_Type2();
            }

        }

    }

    // 原地展示
    void TiaoZiLoop_Type4()
    {
        if (m_eType != CTiaoZiManager.eTiaoZiType.type_4)
        {
            return;
        }

        m_fType4_CurChiXuTime += Time.fixedDeltaTime;
        if (m_fType4_CurChiXuTime > m_fType4_ChiXuTime)
        {
            EndTiaoZi_Type4();
        }
    }

    void TiaoZiLoop_Type1()
    {
        if (m_eType != CTiaoZiManager.eTiaoZiType.type_1)
        {
            return;
        }

        m_fPaoWuXianVy0 += Time.fixedDeltaTime * m_fPaoWuXianA;
        if (m_nStatus == 0)
        {
            if ( m_fPaoWuXianVy0 <= 0)
            {
                m_nStatus = 1;
            }
        }
        if (m_nStatus == 1)
        {
            // alpha渐变
            m_fCurAlpha += m_fPaoWuXianAlphaA * Time.fixedDeltaTime;
            
   
           // SetAlpha(m_fCurAlpha); 

            if (m_vecCurPos.y < m_vecStartPos.y  )
            {
                EndTiaoZi_Type1();
            }
        }



        vecTempPos = GetPos();
        m_vecCurPos.x += m_fPaoWuXianVx0 * Time.fixedDeltaTime;
        m_vecCurPos.y += m_fPaoWuXianVy0 * Time.fixedDeltaTime;
        vecTempPos.x += m_fPaoWuXianVx0 * Time.fixedDeltaTime;
        vecTempPos.y += m_fPaoWuXianVy0 * Time.fixedDeltaTime;
        SetPos(vecTempPos);
		
    }

    void EndTiaoZi_Type1()
    {
        m_bActive = false;
        CTiaoZiManager.s_Instance.DeleteTiaoZi( this );
    }

    void EndTiaoZi_Type2()
    {
        m_bActive = false;
        CTiaoZiManager.s_Instance.DeleteTiaoZi(this);

    }

    void EndTiaoZi_Type3()
    {
        m_bActive = false;
        CTiaoZiManager.s_Instance.DeleteTiaoZi(this);
    }

    void EndTiaoZi_Type4()
    {
        m_bActive = false;
        CTiaoZiManager.s_Instance.DeleteTiaoZi(this);

    }

    void EndTiaoZi_YanMie()
    {
        m_bActive = false;
        CTiaoZiManager.s_Instance.DeleteTiaoZi(this);
    }

    void EndTiaoZi_TunShi()
    {
        m_bActive = false;
        if (m_Ball)
        {
            m_Ball.RemoveTiaoZi_TunShi(m_nIndex);
        }

        CTiaoZiManager.s_Instance.DeleteTiaoZi(this);
    }

    int m_nIndex = 0;
    public void SetIndex( int nIndex )
    {
        m_nIndex = nIndex;
    }
}
