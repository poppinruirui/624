﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WebAuthAPI;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using System.IO;
using System.Linq;
public class CAccountSystem : MonoBehaviour {
   
    public static CAccountSystem s_Instance = null;

    public CanvasScaler _CanvasScaler;

    public Color m_colorButtonEnabledColor;
    public Color m_colorButtonDisabledColor;


    /// <summary>
    /// /UI
    /// </summary>
    //// 各种面版，按设备型号分版本
    public GameObject[] m_aryHomePageContainerAll;
    public GameObject[] m_arySelectSkilPanel;
    public GameObject[] m_aryPanelSettings;


    // 资源加载等待界面
    public CProgressBar _progressLoadIng;
    public GameObject _panelProgressLoadIng;
    public Scrollbar _scollLoadResourcePercent;

    public Image _imgCurEquipedAvatar;
    public Image[] m_aryCurEquipedAvatar;

    // !-- 登录
    public InputField _inputLogin_PhoneNum;
    public InputField _inputLogin_Password;
    public InputField _inputLogin_GoToRegidter;

    // !-- 修改昵称
    public InputField[] m_aryUpdateRoleName;
    //public InputField _inputUpdateRoleName;
    //public InputField _inputUpdateRoleName_Other;

    // !-- 主界面
    public InputField _inputMainUI_RoleName;

    public Text _txtMainUI_RoleName;
    public Text[] m_aryHomePageRoleName;


    public Text[] _aryMoney;
    public CCyberTreeList m_cyberlistRankingList;

    // !-- 注册
    public InputField _inputRegister_PhoneNum;
    public InputField _inputRegister_SmsCode;
    public InputField _inputRegister_Password;
    public Button _btnGetSmsCode;
    public Text _txtButtonSmsCodeCaption;

    // !-- 重置密码
    public InputField _inputResetPassword_PhoneNum;
    public InputField _inputResetPassword_SmsCode;
    public InputField _inputResetPassword_Password;

    public Button _btnGetSmsCode_ResetPwd;
    public Text _txtButtonSmsCodeCaption_ResetPwd;


    // !--临时测试界面
    public InputField _inputTest;

    // ! -- 各种面板的父容器
    public GameObject _panelUpdateRoleName;
    public GameObject[] m_aryUpdateRoleNamePanel;


    public GameObject _panelAccountPasswordLogin;

    public GameObject _panelMainUI;
    public GameObject[] m_aryPanelMainUI;

    public GameObject _panelRegister;
    public GameObject _panelResetPassword;

    /// <summary>
    ///  bottom  buttons
    /// </summary>
    public Text[] m_aryBottomButtons_Text;
    public Image[] m_aryBottomButtons_Image;
    public Color m_colorBottomButton_Selected;
    public Color m_colorBottomButton_NotSelected;
    public Sprite m_sprBottomButton_Selected;
    public Sprite m_sprBottomButton_NotSelected;

    /// <summary>
    /// / prefab
    /// </summary>
    public GameObject m_preRankingListItem;

    /// <summary>
    /// / 电池和网络
    /// </summary>
    public Image _imgBattery;
    public CWifi _wifiInfo;

    public CyberTreeScrollView _listPaiHangBang;

    public static bool m_bNickNameSettled = false;

    ///////////// 狗日的适配
    public enum eCtrlType
    {
        home_page_container_all,
        update_Role_Name_inputfield,
        home_page_panel,
        update_Role_Name_Panle,
        home_page_role_name,
        select_skill_panel,
        home_page_cur_equipped_avatar,
        panel_settings, // 设置面板
    };



    public UnityEngine.Object GetCtrl(CAccountSystem.eCtrlType eType)
    {
        int nDeviceType = (int)CAdaptiveManager.s_Instance.GetCurDeviceType();

        switch (eType)
        {
            case eCtrlType.home_page_container_all:
                {
                    return m_aryHomePageContainerAll[nDeviceType];
                }
                break;
            case CAccountSystem.eCtrlType.update_Role_Name_inputfield:
                {
                    return m_aryUpdateRoleName[nDeviceType];
                }
                break;
            case CAccountSystem.eCtrlType.home_page_panel:
                {
                    return m_aryPanelMainUI[nDeviceType];
                }
                break;
            case CAccountSystem.eCtrlType.update_Role_Name_Panle:
                {
                    return m_aryUpdateRoleNamePanel[nDeviceType];
                }
                break;
            case eCtrlType.home_page_role_name:
                {
                    return m_aryHomePageRoleName[nDeviceType];
                } break;
            case eCtrlType.select_skill_panel:
                {
                    return m_arySelectSkilPanel[nDeviceType];
                }
                break;
            case eCtrlType.home_page_cur_equipped_avatar:
                {
                    return m_aryCurEquipedAvatar[nDeviceType];
                }
                break;
            case eCtrlType.panel_settings:
                {
                    return m_aryPanelSettings[nDeviceType];
                }
                break;

        } // end switch

        return null;
    }


    private void Awake()
    {
        s_Instance = this;
    }

    void DoTest()
    {

    }

    // Use this for initialization
    void Start () {

        DoTest();

        _CanvasScaler.referenceResolution = CAdaptiveManager.s_Instance.GetResolution();
        m_aryHomePageContainerAll[(int)CAdaptiveManager.s_Instance.GetCurDeviceType()].SetActive( true );

        for ( int i = 0; i < 9; i++ )
        {
            CBriefRankingListItem item = NewRankingListItem();
            _listPaiHangBang.AddItem( item.gameObject, 0.8f );
        }

        //WifiInfoLoop();
        _wifiInfo.UpdateWifi(30f);
        _imgBattery.fillAmount = SystemInfo.batteryLevel;

        (GetCtrl(eCtrlType.home_page_container_all) as GameObject).SetActive( true );

    }



    public void Reload()
    {
       // UpdateCoinInfo();
    }

    // Update is called once per frame
    void Update () {



        WifiInfoLoop();
    }

    float m_fWifiTimeElapse = 0f;
    void WifiInfoLoop()
    {
        m_fWifiTimeElapse += Time.deltaTime;
        if (m_fWifiTimeElapse < 3f)
        {
            return;
        }
        m_fWifiTimeElapse = 0f;

    //Ping();
       
    }

    public CCyberTreePoPo _popoStreetNews;
    public CCyberTreePoPo _popoHotEvents;
    public void SendStreetNews( string szContent )
    {
        _popoStreetNews.SetContent(szContent);
        _popoStreetNews.SetActive( true );
    }

    public void SendHotEvents(string szContent)
    {
        _popoHotEvents.SetContent(szContent);
        _popoHotEvents.SetActive(true);
    }

    public CBriefRankingListItem NewRankingListItem()
    {
        return GameObject.Instantiate(m_preRankingListItem).GetComponent< CBriefRankingListItem>();
    }

    float m_fPingTime = 0; 
    public void Ping()
    {
        string host = "ns-cn.exitgames.com";
        System.Net.NetworkInformation.Ping p1 = new System.Net.NetworkInformation.Ping();
        System.Net.NetworkInformation.PingReply reply = p1.Send(host); //发送主机名或Ip地址  
        if (reply.Status == System.Net.NetworkInformation.IPStatus.Success)
        {
            /*
            sbuilder.AppendLine(string.Format("Address: {0} ", reply.Address.ToString()));
            sbuilder.AppendLine(string.Format("RoundTrip time: {0} ", reply.RoundtripTime));
            sbuilder.AppendLine(string.Format("Time to live: {0} ", reply.Options.Ttl));
            sbuilder.AppendLine(string.Format("Don't fragment: {0} ", reply.Options.DontFragment));
            sbuilder.AppendLine(string.Format("Buffer size: {0} ", reply.Buffer.Length));
            */
            m_fPingTime = reply.RoundtripTime;
            _wifiInfo.UpdateWifi(m_fPingTime);
        }
        else 
        {
            m_fPingTime = 1000f;
        }
    }
   
    public void SelectBottomButton( int nButtonIndex )
    {
        return;

        for ( int i = 0; i < m_aryBottomButtons_Text.Length; i++ )
        {
            if ( i == nButtonIndex)
            {
                m_aryBottomButtons_Text[i].color = m_colorBottomButton_Selected;
                m_aryBottomButtons_Image[i].sprite = m_sprBottomButton_Selected;
            }
            else
            {
                m_aryBottomButtons_Text[i].color = m_colorBottomButton_NotSelected;
                m_aryBottomButtons_Image[i].sprite = m_sprBottomButton_NotSelected;
            }
        }
    }

    public void OnClickButton_BottonButton0()
    {
        SelectBottomButton(0);
    }

    public void OnClickButton_BottonButton1()
    {
        SelectBottomButton(1);
        AccountManager.s_Instance.OnClickButton_GoToShoppinMall();
    }

    public void OnClickButton_BottonButton2()
    {
        SelectBottomButton(2);
    }


    public void OnClickButton_BottonButton3()
    {
        SelectBottomButton(3);
    }


    public void OnClickButton_BottonButton4()
    {
        SelectBottomButton(4);
    }

    public void OnClickButton_RegisterPrevTo()
    {
        _panelAccountPasswordLogin.SetActive( true );
        _panelRegister.SetActive(false);
    }


    public void OnClickButton_ResetPasswordPrevTo()
    {
        _panelAccountPasswordLogin.SetActive(true);
        _panelResetPassword.SetActive(false);
    }

    public void OnClickButton_Settings()
    {
        SetPanelSettingsVisible( true );
    }

    public void SetPanelSettingsVisible( bool bVisible )
    {
        (GetCtrl(eCtrlType.panel_settings) as GameObject).SetActive(bVisible);
    }

}

